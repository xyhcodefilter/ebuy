/*
 Navicat Premium Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : localhost:3306
 Source Schema         : ebuy

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 09/06/2021 11:21:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easybuy_product
-- ----------------------------
DROP TABLE IF EXISTS `easybuy_product`;
CREATE TABLE `easybuy_product`  (
  `EP_ID` int(10) NOT NULL AUTO_INCREMENT,
  `EP_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EP_DESCRIPTION` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `EP_PRICE` float NOT NULL,
  `EP_STOCK` int(10) NOT NULL,
  `EPC_CHILD_ID` int(10) NULL DEFAULT NULL,
  `EP_FILE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`EP_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 697 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of easybuy_product
-- ----------------------------
INSERT INTO `easybuy_product` VALUES (674, '保鲜盒', '乐扣普通型保鲜盒圣诞7件套', 69.9, 6678, 15, '1623208403732.jpg');
INSERT INTO `easybuy_product` VALUES (675, '欧珀莱', '欧珀莱均衡保湿四件套', 279, 8354, 8, '1623038987281.jpg');
INSERT INTO `easybuy_product` VALUES (676, '笔记本电脑', '联想笔记本电脑 高速独立显存', 4199, 5655, 16, '4.jpg');
INSERT INTO `easybuy_product` VALUES (677, '牛仔裤', '法姿韩版显瘦彩边时尚牛仔铅笔裤', 49, 3365, 6, '5.jpg');
INSERT INTO `easybuy_product` VALUES (678, '吊坠', 'Genius925纯银施华洛世奇水晶吊坠', 69.9, 5837, 12, '6.jpg');
INSERT INTO `easybuy_product` VALUES (679, '电饼铛', '利仁2018M福满堂电饼铛 好用实惠', 268, 8354, 15, '7.jpg');
INSERT INTO `easybuy_product` VALUES (680, '拉杆箱', '达派高档拉杆箱20寸 经典款式', 198, 4367, 5, '8.jpg');
INSERT INTO `easybuy_product` VALUES (681, 'MP4', '爱国者MP4 全屏触摸多格式播放 4G', 289, 6678, 16, '9.jpg');
INSERT INTO `easybuy_product` VALUES (682, '奶粉', '多美滋金装金盾3阶段幼儿配方奶粉', 186, 3455, 9, '10.jpg');
INSERT INTO `easybuy_product` VALUES (686, '根雕', '教你制作魅力的根雕作品', 50, 100, 3, '1622861209885.jpg');
INSERT INTO `easybuy_product` VALUES (687, '幸福的科学', '教你把控幸福', 67, 199, 3, '1622861434682.jpg');
INSERT INTO `easybuy_product` VALUES (688, '意林', '《意林》以“小故事大智慧、小幽默大道理、小视角大意境”，《意林》杂志不仅为广大读者提供丰富的心灵鸡汤和人生智慧，而向全社会制造并传播正能量，更以广博的素材信息量帮助中学生决胜中高考作文，深受广大读者的青睐，影响并建设了亿万中国人正而积极的精神世界', 24, 100, 1, '1622946933329.jpg');
INSERT INTO `easybuy_product` VALUES (690, 'LD 玛蒂尼小提琴', 'LD 玛蒂尼KA德国大师制演奏级精品小提琴手工乐器定制款', 432038, 100, 2, '1622951828685.png');
INSERT INTO `easybuy_product` VALUES (691, '戴尔dell成就3690升级版', '戴尔dell成就3690升级版英特尔酷睿i3商用台式机电脑主机(i3-10105 8G 1T 三年服务)+21.5英寸电脑显示器', 3149, 100, 16, '1622952985937.png');
INSERT INTO `easybuy_product` VALUES (693, '荣耀MagicBook Pro 2020款', '荣耀MagicBook Pro 2020款 16.1英寸全面屏轻薄笔记本电脑（R5 4600H 16G 512G 7nm 100%sRGB）冰河银', 4399, 100, 1, '1622954102783.png');
INSERT INTO `easybuy_product` VALUES (694, '食用油', '菜籽油', 67, 100, 10, '1623038725865.jpg');
INSERT INTO `easybuy_product` VALUES (696, '废报纸', '旧报纸报纸废旧报纸填充纸包装', 10, 100, 4, '1623052451701.jpg');

SET FOREIGN_KEY_CHECKS = 1;
