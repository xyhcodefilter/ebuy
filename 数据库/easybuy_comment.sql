/*
 Navicat Premium Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : localhost:3306
 Source Schema         : ebuy

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 09/06/2021 11:19:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easybuy_comment
-- ----------------------------
DROP TABLE IF EXISTS `easybuy_comment`;
CREATE TABLE `easybuy_comment`  (
  `EC_ID` int(10) NOT NULL AUTO_INCREMENT,
  `EC_CONTENT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EC_CREATE_TIME` date NOT NULL,
  `EC_REPLY` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EC_REPLY_TIME` datetime NULL DEFAULT NULL,
  `EC_NICK_NAME` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`EC_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 691 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of easybuy_comment
-- ----------------------------
INSERT INTO `easybuy_comment` VALUES (655, '刚订了台IPod，很是期待啊', '2020-12-22', '货已发出，请注意收货时开箱检查货物是否有问题', '2021-06-02 16:53:33', '小乖');
INSERT INTO `easybuy_comment` VALUES (680, '佳能D50现在可以多长时间发货呢', '2020-12-24', '一般在订单确认后的第3天发货', '2011-02-24 00:00:00', '无极');
INSERT INTO `easybuy_comment` VALUES (681, 'iphone内存大嘛？', '2021-01-13', '内存有多种选择', '2021-05-30 15:38:00', '小怪');
INSERT INTO `easybuy_comment` VALUES (682, '荣耀笔记本电脑会发烫嘛', '2021-02-17', '一般不会哦', '2021-05-28 15:38:59', '小颖');
INSERT INTO `easybuy_comment` VALUES (683, 'iphone的内存大嘛', '2019-10-18', '不大，好垃圾！', '2020-10-18 17:50:21', '羡羡');
INSERT INTO `easybuy_comment` VALUES (687, '山东的苹果好次嘛', '2021-01-06', '不好次！', '2021-06-03 10:49:52', '李华');
INSERT INTO `easybuy_comment` VALUES (688, '日本的樱花好看嘛', '2018-10-09', '你好', '2021-06-09 10:04:11', '颖颖');
INSERT INTO `easybuy_comment` VALUES (689, '荣耀笔记本会发烫嘛？', '2021-06-07', '会', '2021-06-09 10:39:23', '肖杨辉');
INSERT INTO `easybuy_comment` VALUES (690, '地方', '2021-06-09', '哈哈哈', '2021-06-09 10:41:14', '李思思');

SET FOREIGN_KEY_CHECKS = 1;
