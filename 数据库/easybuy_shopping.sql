/*
 Navicat Premium Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : localhost:3306
 Source Schema         : ebuy

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 09/06/2021 11:21:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easybuy_shopping
-- ----------------------------
DROP TABLE IF EXISTS `easybuy_shopping`;
CREATE TABLE `easybuy_shopping`  (
  `EOD_ID` int(10) NOT NULL AUTO_INCREMENT,
  `EO_USER_ID` int(10) NOT NULL,
  `EP_ID` int(10) NOT NULL,
  `EOD_QUANTITY` int(6) NOT NULL,
  `EOD_COST` float NOT NULL,
  `EO_STATE` int(11) NOT NULL,
  PRIMARY KEY (`EOD_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
