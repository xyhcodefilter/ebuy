/*
 Navicat Premium Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : localhost:3306
 Source Schema         : ebuy

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 09/06/2021 11:21:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easybuy_news
-- ----------------------------
DROP TABLE IF EXISTS `easybuy_news`;
CREATE TABLE `easybuy_news`  (
  `EN_ID` int(10) NOT NULL AUTO_INCREMENT,
  `EN_TITLE` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EN_CONTENT` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EN_CREATE_TIME` date NOT NULL,
  PRIMARY KEY (`EN_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 664 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of easybuy_news
-- ----------------------------
INSERT INTO `easybuy_news` VALUES (529, '最新酷睿笔记本', 'IBME系列全场促销中，最新酷睿双核处理器，保证CPU更高效的运转。', '2020-12-23');
INSERT INTO `easybuy_news` VALUES (530, '团购无忧', '团购无忧', '2020-12-22');
INSERT INTO `easybuy_news` VALUES (531, '会员特惠月开始了', '会员特惠月开始了', '2020-12-22');
INSERT INTO `easybuy_news` VALUES (597, '迎双旦促销大酬宾', '迎双旦促销大酬宾', '2020-12-24');
INSERT INTO `easybuy_news` VALUES (649, '加入会员，赢千万大礼包', '加入会员，赢千万大礼包', '2020-12-22');
INSERT INTO `easybuy_news` VALUES (650, '新年不夜天，通宵也是开张了', '新年不夜天，通宵也是开张了', '2020-12-22');
INSERT INTO `easybuy_news` VALUES (651, '积分兑换开始了', '积分兑换开始了', '2020-12-22');
INSERT INTO `easybuy_news` VALUES (653, '团购阿迪1折起', '团购阿迪1折起', '2020-12-22');
INSERT INTO `easybuy_news` VALUES (654, '配货通知', '配货通知', '2020-12-22');
INSERT INTO `easybuy_news` VALUES (655, 'iphone手机免费送', 'iPhone手机9.9 包邮到家', '2020-09-16');
INSERT INTO `easybuy_news` VALUES (656, '口罩大甩卖', '口罩10元20个 清仓大甩卖', '2021-04-29');
INSERT INTO `easybuy_news` VALUES (658, '挖矿机大减价', '挖矿机每台减价2000元', '2021-05-31');
INSERT INTO `easybuy_news` VALUES (659, '最新品电脑', '最新电脑华硕@豆', '2019-01-10');
INSERT INTO `easybuy_news` VALUES (661, 'iphone 13新上架', 'iphone新上架 首周满10000打9折', '2021-06-02');
INSERT INTO `easybuy_news` VALUES (662, '时间', '你好啊', '2021-06-09');
INSERT INTO `easybuy_news` VALUES (663, '你好', '今天项目演示', '2021-06-09');

SET FOREIGN_KEY_CHECKS = 1;
