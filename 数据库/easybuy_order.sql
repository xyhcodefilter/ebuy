/*
 Navicat Premium Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : localhost:3306
 Source Schema         : ebuy

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 09/06/2021 11:21:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easybuy_order
-- ----------------------------
DROP TABLE IF EXISTS `easybuy_order`;
CREATE TABLE `easybuy_order`  (
  `EO_ID` int(10) NOT NULL AUTO_INCREMENT,
  `EO_USER_ID` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EO_USER_NAME` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EO_USER_TEL` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EO_USER_ADDRESS` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EO_COST` float NOT NULL,
  `EO_CREATE_TIME` date NOT NULL,
  `EO_STATUS` int(6) NOT NULL,
  `EO_TYPE` int(6) NOT NULL,
  PRIMARY KEY (`EO_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60235 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of easybuy_order
-- ----------------------------
INSERT INTO `easybuy_order` VALUES (60226, '66', '李思思', '12198378136', '湖北武汉', 24, '2021-06-07', 1, 2);
INSERT INTO `easybuy_order` VALUES (60229, '72', '肖杨辉', '18917612812', '广州花都区濂溪路29号', 4199, '2021-06-07', 1, 2);
INSERT INTO `easybuy_order` VALUES (60232, '66', '李思思', '12198378136', '湖北武汉', 432112, '2021-06-07', 1, 2);
INSERT INTO `easybuy_order` VALUES (60233, '66', '李思思', '12198378136', '湖北武汉', 49, '2021-06-09', 1, 2);
INSERT INTO `easybuy_order` VALUES (60234, '66', '李思思', '12198378136', '湖北武汉', 49, '2021-06-09', 1, 2);

SET FOREIGN_KEY_CHECKS = 1;
