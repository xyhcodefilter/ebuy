/*
 Navicat Premium Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : localhost:3306
 Source Schema         : ebuy

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 09/06/2021 11:21:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easybuy_user
-- ----------------------------
DROP TABLE IF EXISTS `easybuy_user`;
CREATE TABLE `easybuy_user`  (
  `EU_USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EU_USER_Account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EU_USER_NAME` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EU_PASSWORD` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EU_SEX` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EU_BIRTHDAY` date NULL DEFAULT NULL,
  `EU_IDENTITY_CODE` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EU_EMAIL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EU_MOBILE` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EU_ADDRESS` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EU_STATUS` int(6) NOT NULL,
  PRIMARY KEY (`EU_USER_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of easybuy_user
-- ----------------------------
INSERT INTO `easybuy_user` VALUES (1, 'zhangsan', '张三', '12345', '男', '2001-02-08', '360267200910141114', 'lisi@outlook.com', '15609822765', '北京市朝阳区广福路27号', 1);
INSERT INTO `easybuy_user` VALUES (2, 'admin', '李华', 'admin', '女', '2015-09-30', '234567200110241452', 'admin@outlook.com', '14509246543', '广州白云区濂溪路29号', 2);
INSERT INTO `easybuy_user` VALUES (66, 'lisisi', '李思思', 'sisi', '女', '2007-02-01', '320102200702011112', 'sisi@outlook.com', '12198378136', '湖北武汉', 1);
INSERT INTO `easybuy_user` VALUES (67, 'lisi', '李四', '12345', '男', '2003-10-14', '360231300310141112', 'lisi@outlook.com', '18309872451', '广东省广州市花都区', 2);
INSERT INTO `easybuy_user` VALUES (68, 'huangbin', '黄斌', '1234', '女', '2021-06-01', '370123896528391313', 'xaudyad@adad', '18917612812', '广州花都区濂溪路29号', 1);
INSERT INTO `easybuy_user` VALUES (69, 'lisi', '李思', '12345', '女', '2001-10-01', '387221200110015431', 'lisi@outlook.com', '18709213321', '广州市白云区', 1);
INSERT INTO `easybuy_user` VALUES (71, 'fangdodo', '方泽泽', '1234542', '男', '2008-01-01', '121888166919118667', 'fangdodo@outlook.com', '91912112121', '广东江门', 1);
INSERT INTO `easybuy_user` VALUES (72, 'rrr', '张三三', '111', '男', '2021-06-04', '370123896528391313', 'xaudyad@adad', '18917612812', '广州花都区濂溪路29号', 1);
INSERT INTO `easybuy_user` VALUES (73, 'ttt', '张三三三', '111', '男', '2021-06-11', '370123896528391313', 'xaudyad@adad', '18917612812', '广州花都区濂溪路29号', 2);

SET FOREIGN_KEY_CHECKS = 1;
