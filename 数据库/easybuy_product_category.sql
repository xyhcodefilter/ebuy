/*
 Navicat Premium Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : localhost:3306
 Source Schema         : ebuy

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 09/06/2021 11:21:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easybuy_product_category
-- ----------------------------
DROP TABLE IF EXISTS `easybuy_product_category`;
CREATE TABLE `easybuy_product_category`  (
  `EPC_ID` int(10) NOT NULL AUTO_INCREMENT,
  `EPC_NAME` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EPC_PARENT_ID` int(10) NOT NULL,
  PRIMARY KEY (`EPC_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of easybuy_product_category
-- ----------------------------
INSERT INTO `easybuy_product_category` VALUES (1, '杂志', 1);
INSERT INTO `easybuy_product_category` VALUES (2, '音乐', 1);
INSERT INTO `easybuy_product_category` VALUES (3, '书籍', 1);
INSERT INTO `easybuy_product_category` VALUES (4, '报纸', 1);
INSERT INTO `easybuy_product_category` VALUES (5, '运动健康', 2);
INSERT INTO `easybuy_product_category` VALUES (6, '服装', 2);
INSERT INTO `easybuy_product_category` VALUES (7, '家居', 2);
INSERT INTO `easybuy_product_category` VALUES (8, '美妆', 2);
INSERT INTO `easybuy_product_category` VALUES (9, '母婴', 2);
INSERT INTO `easybuy_product_category` VALUES (10, '食品', 2);
INSERT INTO `easybuy_product_category` VALUES (11, '手机数码', 2);
INSERT INTO `easybuy_product_category` VALUES (12, '家居首饰', 2);
INSERT INTO `easybuy_product_category` VALUES (13, '手表饰品', 2);
INSERT INTO `easybuy_product_category` VALUES (14, '鞋包', 2);
INSERT INTO `easybuy_product_category` VALUES (15, '家电', 2);
INSERT INTO `easybuy_product_category` VALUES (16, '电脑办公', 2);
INSERT INTO `easybuy_product_category` VALUES (17, '玩具文具', 2);

SET FOREIGN_KEY_CHECKS = 1;
