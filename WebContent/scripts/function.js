//  JavaScript Document
window.onload = function(){
	showChater();
	scrollChater();
}
window.onscroll = scrollChater;
window.onresize = scrollChater;

function FocusItem(obj)
{
	obj.parentNode.parentNode.className = "current";
	var msgBox = obj.parentNode.getElementsByTagName("span")[0];
	msgBox.innerHTML = "";
	msgBox.className = "";
}

function CheckItem(obj)
{
	obj.parentNode.parentNode.className = "";
	var msgBox = obj.parentNode.getElementsByTagName("span")[0];
	switch(obj.name) {
		case "userName":
			if(obj.value == "") {
				msgBox.innerHTML = "姓名不能为空";
				msgBox.className = "error";
				return false;
			}
			break;
		case "userAc":
			if(obj.value == "") {
				msgBox.innerHTML = "用户名不能为空";
				msgBox.className = "error";
				return false;
			}
			break;
		case "passWord":
			if(obj.value == "") {
				msgBox.innerHTML = "密码不能为空";
				msgBox.className = "error";
				return false;
			}
			break;
		case "rePassWord":
			if(obj.value == "") {
				msgBox.innerHTML = "确认密码不能为空";
				msgBox.className = "error";
				return false;
			} else if(obj.value != document.getElementById("passWord").value) {
				msgBox.innerHTML = "两次输入的密码不相同";
				msgBox.className = "error";
				return false;
			}
			break;
		case "veryCode":
			if(obj.value == "") {
				msgBox.innerHTML = "验证码不能为空";
				msgBox.className = "error";
				return false;
			}
			break;
		case "userBirthday":
		    if(obj.value==""){
			msgBox.innerHTML = "出生日期不能为空";
				msgBox.className = "error";
				return false;
		}
		break;
		case "userIdentity":
		 if(obj.value==""){
			msgBox.innerHTML = "身份证不能为空";
				msgBox.className = "error";
				return false;
		}
		break;
		case "userEmail":
		if(obj.value==""){
			msgBox.innerHTML = "电子邮件不能为空";
				msgBox.className = "error";
				return false;
		}else if(obj.value.indexOf("@")==-1){
			msgBox.innerHTML = "电子邮件必须包含@符号";
				msgBox.className = "error";
				return false;
		}
		break;
		case "userMobile":
		if(obj.value==""){
			msgBox.innerHTML = "电话不能为空";
				msgBox.className = "error";
				return false;
		}
		break;
		case "userAddress":
		if(obj.value==""){
			msgBox.innerHTML = "地址不能为空";
				msgBox.className = "error";
				return false;
		}
		break;
	}
	return true;
}

function checkForm(frm)
{
	var els = frm.getElementsByTagName("input");
	for(var i=0; i<els.length; i++) {
		if(typeof(els[i].getAttribute("onblur")) == "function") {
			if(!CheckItem(els[i])) return false;
		}
	}
	return true;
}

function showChater()
{
	var _chater = document.createElement("div");
	_chater.setAttribute("id", "chater");
	var _dl = document.createElement("dl");
	var _dt = document.createElement("dt");
	var _dd = document.createElement("dd");
	var _a = document.createElement("a");
	_a.setAttribute("href", "https://bots.4paradigm.com/web/chat/29299/38e78e08-54f7-42dc-b5d6-32ab41aeb820?color=%23E6F0FB-%23354052-%23f7f8fa-%233ba1ff-%23a8b7c7");
	_a.appendChild(document.createTextNode("在线客服"));
	_dd.appendChild(_a);
	_dl.appendChild(_dt);
	_dl.appendChild(_dd);
	_chater.appendChild(_dl);
	document.body.appendChild(_chater);
}


function scrollChater()
{
	var chater = document.getElementById("chater");
	if(chater!=null){
		var scrollTop = document.documentElement.scrollTop;
		var scrollLeft = document.documentElement.scrollLeft;
		chater.style.left = scrollLeft + document.documentElement.clientWidth - 92 + "px";
		chater.style.top = scrollTop + document.documentElement.clientHeight - 25 + "px";
	}
}

function inArray(array, str)
{
	for(a in array) {
		if(array[a] == str) return true;
	}
	return false;
}

function setCookie(name,value)
{
  var Days = 30;
  var exp  = new Date();
  exp.setTime(exp.getTime() + Days*24*60*60*1000);
  document.cookie = name + "="+ escape(value) +";expires="+ exp.toGMTString();
}

function getCookie(name)
{
  var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
  if(arr != null) return unescape(arr[2]); return null;
}

function delCookie(name)
{
  var exp = new Date();
  exp.setTime(exp.getTime() - 1);
  var cval=getCookie(name);
  if(cval!=null) document.cookie=name +"="+cval+";expires="+exp.toGMTString();
}

function goBuy(id, price)
{
	var newCookie = "";
	var oldCookie = getCookie("product");
	if(oldCookie) {
		if(inArray(oldCookie.split(","), id)) {
			newCookie = oldCookie;
		} else {
			newCookie = id + "," + oldCookie;
		}
	} else {
		newCookie = id;
	}
	setCookie("product", newCookie);
	location.href = "shopping.jsp";
}

function delShopping(id)
{
	var tr = document.getElementById("product_id_"+ id);
	var oldCookie = getCookie("product");
	if(oldCookie) {
		var oldCookieArr = oldCookie.split(",");
		var newCookieArr = new Array();
		for(c in oldCookieArr) {
			var cookie = parseInt(oldCookieArr[c]);
			if(cookie != id) newCookieArr.push(cookie);
		}
		var newCookie = newCookieArr.join(",");
		setCookie("product", newCookie);
	}
	if(tr) tr.parentNode.removeChild(tr);
	location.href="/Ebuy/Shopping/del?gid="+id;
}

function reloadPrice(id, status)
{
	var price = document.getElementById("price_id_" + id).getElementsByTagName("input")[0].value;
	var priceBox = document.getElementById("price_id_" + id).getElementsByTagName("span")[0];
	var number = document.getElementById("number_id_" + id);
	if(status) {
		number.value++;
		if(number.value>999){
			number.value=999;
		}
	} else {
		if(number.value == 1) {
			return false;
		} else {
			number.value--;
		}
	}
	var dj=price * number.value;
	priceBox.innerHTML =dj.toFixed(2);
	
	var jg = document.querySelectorAll(".jg");  //得到每个价格
	var total = document.querySelector("#total"); 
	var num = 0;
	for(var i=0;i<jg.length;i++){
		num+=parseFloat(jg[i].innerHTML);
		
	}
	//console.log(num);
	total.innerHTML= num.toFixed(2); 
}