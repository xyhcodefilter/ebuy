<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.Date"%>
<%@ page import="java.text.*"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> 后台管理 - 易买网</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function-manage.js"></script>
<style >
#a1{
background-color:#4E6EF2;
color:#FFFFFF;
}

#back,#nextpage{
padding:5px 5px;
text-decoration:none;
font-size:14px;
border-radius:5%;
color:#3951B3;
background-color:#f5f5f5;
}
</style>
</head>
<body>
<div id="header" class="wrap">
	<div id="logo"><img src="../images/logo.gif" /></div>
	<div class="help"><a href="/Ebuy/frontend/index.jsp">返回前台页面</a></div>
	<div class="navbar">
		<ul class="clearfix">
			<li><a href="/Ebuy/manage/index.jsp">首页</a></li>
			<li><a href="/Ebuy/UserManServlet/muse">用户</a></li>
			<li><a href="/Ebuy/Product/query">商品</a></li>
			<li><a href="/Ebuy/OrderManaServlet/osef">订单</a></li>
			<li><a href="/Ebuy/EnquiryManaServlet/seenqfind">留言</a></li>
			<li class="current"><a href="/Ebuy/NewsManServlet/newfind">新闻</a></li>
		</ul>
	</div>
</div>
<div id="childNav">
	<%
String name=(String) request.getSession().getAttribute("adname");
String drna=null;
String time=null;
if(name!=null){
	drna=name;
	Date nowdate=new Date();
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	time=sdf.format(nowdate);
}else{
	drna="pillys";
	Date nowdate=new Date();
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	time=sdf.format(nowdate);
}
%>
	<div class="welcome wrap">
		管理员 <%=drna %> 您好，今天是<%=time %>，欢迎回到管理后台。
	</div>
</div>
<div id="position" class="wrap">
	您现在的位置：<a href="/Ebuy/manage/index.jsp">易买网</a> &gt; 管理后台
</div>
<div id="main" class="wrap">
	<div id="menu-mng" class="lefter">
		<div class="box">
			<dl>
				<dt>用户管理</dt>
				<dd><em><a href="/Ebuy/manage/user-add.jsp">新增</a></em><a href="/Ebuy/UserManServlet/muse">用户管理</a></dd>
				<dt>商品信息</dt>
				<dd><em><a href="/Ebuy/manage/productClass-add.jsp">新增</a></em><a href="/Ebuy/Category/query">分类管理</a></dd>
				<dd><em><a href="/Ebuy/manage/product-add.jsp">新增</a></em><a href="/Ebuy/Product/query">商品管理</a></dd>
				<dt>订单管理</dt>
				<dd><a href="/Ebuy/OrderManaServlet/osef">订单管理</a></dd>
				<dt>留言管理</dt>
				<dd><a href="/Ebuy/EnquiryManaServlet/seenqfind">留言管理</a></dd>
				<dt>新闻管理</dt>
				<dd><em><a href="/Ebuy/manage/news-add.jsp">新增</a></em><a href="/Ebuy/NewsManServlet/newfind">新闻管理</a></dd>
			</dl>
		</div>
	</div>
	<div class="main">
		<h2>新闻管理</h2>
		<div class="manage">
			<table class="list">
				<tr>
					<th>ID</th>
					<th>新闻标题</th>
					<th>操作</th>
				</tr>
				<c:forEach items="${newlis}" var="n">
				<tr>
					<td class="first w4 c">${n.EN_ID }</td>
					<td>${n.EN_TITLE }</td>
					<td class="w1 c"><a href="/Ebuy/NewsManServlet/senewcof?upeid=${n.EN_ID }">修改</a> <a href="/Ebuy/NewsManServlet/dene?neid=${n.EN_ID }" onclick="return confirm('是否删除？')">删除</a></td>
				</tr>
				</c:forEach>
			</table>
			<div class="pager">
					<ul class="clearfix">
					<c:if test="${pagebean.curpage>1 }">
					<li><a  href="/Ebuy/NewsManServlet/newfind?p=1">首页</a></li>
					<li><a href="/Ebuy/NewsManServlet/newfind?p=${pagebean.curpage-1 }">上一页</a></li>
					</c:if>
					<c:forEach begin="${pagebean.begin}" end="${pagebean.end}" var="k">
					<c:set var="p" value="${k==pagebean.curpage?'a1':''}"></c:set>
					<li id="${p }"><a class="a1" href='/Ebuy/NewsManServlet/newfind?p=${k }'>${k}</a></li>
		       </c:forEach>
		       <c:if test="${pagebean.curpage<pagebean.page }">
		       <li><a href="/Ebuy/NewsManServlet/newfind?p=${pagebean.curpage+1 }">下一页</a></li>
		       <li><a href="/Ebuy/NewsManServlet/newfind?p=${pagebean.page}">尾页</a></li>
		       </c:if>
				</ul>
				</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div id="footer">
	Copyright &copy; 2016 九云IT教育 All Rights Reserved. 京ICP证1000001号
</div>
</body>
</html>
