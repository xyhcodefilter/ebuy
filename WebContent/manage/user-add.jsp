<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.Date"%>
<%@ page import="java.text.*"%>
    	 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> 后台管理 - 易买网</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function-manage.js"></script>
<style >
#a1{
background-color:#4E6EF2;
color:#FFFFFF;
}

#back,#nextpage{
padding:5px 5px;
text-decoration:none;
font-size:14px;
border-radius:5%;
color:#3951B3;
background-color:#f5f5f5;
}
</style>
</head>
<body>
<div id="header" class="wrap">
	<div id="logo"><img src="../images/logo.gif" /></div>
	<div class="help"><a href="/Ebuy/frontend/index.jsp">返回前台页面</a></div>
	<div class="navbar">
		<ul class="clearfix">
			<li><a href="/Ebuy/manage/index.jsp">首页</a></li>
			<li class="current"><a href="/Ebuy/UserManServlet/muse">用户</a></li>
			<li><a href="/Ebuy/Product/query">商品</a></li>
			<li><a href="/Ebuy/OrderManaServlet/osef">订单</a></li>
			<li><a href="/Ebuy/EnquiryManaServlet/seenqfind">留言</a></li>
			<li><a href="/Ebuy/NewsManServlet/newfind">新闻</a></li>
		</ul>
	</div>
</div>
<div id="childNav">
	<%
String name=(String) request.getSession().getAttribute("adname");
String drna=null;
String time=null;
if(name!=null){
	drna=name;
	Date nowdate=new Date();
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	time=sdf.format(nowdate);
}else{
	drna="pillys";
	Date nowdate=new Date();
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	time=sdf.format(nowdate);
}
%>
	<div class="welcome wrap">
		管理员 <%=drna %> 您好，今天是<%=time %>，欢迎回到管理后台。
	</div>
</div>
<div id="position" class="wrap">
	您现在的位置：<a href="/Ebuy/manage/index.jsp">易买网</a> &gt; 管理后台
</div>
<div id="main" class="wrap">
	<div id="menu-mng" class="lefter">
		<div class="box">
			<dl>
				<dt>用户管理</dt>
				<dd><em><a href="/Ebuy/manage/user-add.jsp">新增</a></em><a href="/Ebuy/UserManServlet/muse">用户管理</a></dd>
				<dt>商品信息</dt>
				<dd><em><a href="/Ebuy/manage/productClass-add.jsp">新增</a></em><a href="/Ebuy/Category/query">分类管理</a></dd>
				<dd><em><a href="/Ebuy/manage/product-add.jsp">新增</a></em><a href="/Ebuy/Product/query">商品管理</a></dd>
				<dt>订单管理</dt>
				<dd><a href="/Ebuy/OrderManaServlet/osef">订单管理</a></dd>
				<dt>留言管理</dt>
				<dd><a href="/Ebuy/EnquiryManaServlet/seenqfind">留言管理</a></dd>
				<dt>新闻管理</dt>
				<dd><em><a href="/Ebuy/manage/news-add.jsp">新增</a></em><a href="/Ebuy/NewsManServlet/newfind">新闻管理</a></dd>
			</dl>
		</div>
	</div>
	<div class="main">
		<h2>新增用户</h2>
		<div class="manage">
			<form action="/Ebuy/UserManServlet/adse" method="post">
				<table class="form">
					<tr>
						<td class="field">用户名：</td>
						<td><input type="text" class="text" name="userName" value="" /></td>
					</tr>
					<tr>
						<td class="field">姓名：</td>
						<td><input type="text" class="text" name="name" value="" /></td>
					</tr>
					<tr>
						<td class="field">密码：</td>
						<td><input type="password" class="text" name="passWord" value="" /></td>
					</tr>
					<tr>
						<td class="field">性别：</td>
						<td><input type="radio" name="sex" value="男" checked="checked" />男 <input type="radio" name="sex" value="" />女</td>
					</tr>
					<tr>
						<td class="field">出生日期：</td>
						<td>
						 <input type="date"  name="timer"/>
						</td>
					</tr>
					<tr>
						<td class="field">手机号码：</td>
						<td><input type="text" class="text" name="mobile" maxlength="11" value="" /></td>
					</tr>
					<tr>
						<td class="field">身份证：</td>
						<td><input type="text" class="text" maxlength="18" name="cardid"  value="" /></td>
					</tr>
					<tr>
						<td class="field">邮箱：</td>
						<td><input type="email" class="text" name="emal" value="" /></td>
					</tr>
					<tr>
						<td class="field">送货地址：</td>
						<td><input type="text" class="text" name="address" value="" /></td>
					</tr>
					
					<tr>
						<td></td>
						<td><label class="ui-blue"><input type="submit" name="submit" value="添加" /></label></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div id="footer">
	Copyright &copy; 2016 九云IT教育 All Rights Reserved. 京ICP证1000001号
</div>
</body>
</html>
