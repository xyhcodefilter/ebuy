<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.Date"%>
<%@ page import="java.text.*"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
     <%@ page import="com.dao.impl.ProductCategoryDaoimpl"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> 后台管理 - 易买网</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function-manage.js"></script>
</head>
<body>
<div id="header" class="wrap">
	<div id="logo"><img src="../images/logo.gif" /></div>
	<div class="help"><a href="/Ebuy/frontend/index.jsp">返回前台页面</a></div>
	<div class="navbar">
		<ul class="clearfix">
			<li><a href="/Ebuy/manage/index.jsp">首页</a></li>
			<li><a href="/Ebuy/UserManServlet/muse">用户</a></li>
			<li class="current"><a href="/Ebuy/Product/query">商品</a></li>
			<li><a href="/Ebuy/OrderManaServlet/osef">订单</a></li>
			<li><a href="/Ebuy/EnquiryManaServlet/seenqfind">留言</a></li>
			<li><a href="/Ebuy/NewsManServlet/newfind">新闻</a></li>
		</ul>
	</div>
</div>
<div id="childNav">
	<%
String name=(String) request.getSession().getAttribute("adname");
String drna=null;
String time=null;
if(name!=null){
	drna=name;
	Date nowdate=new Date();
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	time=sdf.format(nowdate);
}else{
	drna="pillys";
	Date nowdate=new Date();
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	time=sdf.format(nowdate);
}
%>
	<div class="welcome wrap">
		管理员 <%=drna %> 您好，今天是<%=time %>，欢迎回到管理后台。
	</div>
</div>
<div id="position" class="wrap">
	您现在的位置：<a href="/Ebuy/manage/index.jsp">易买网</a> &gt; 管理后台
</div>
<div id="main" class="wrap">
	<div id="menu-mng" class="lefter">
		<div class="box">
			<dl>
				<dt>用户管理</dt>
				<dd><em><a href="/Ebuy/manage/user-add.jsp">新增</a></em><a href="/Ebuy/UserManServlet/muse">用户管理</a></dd>
				<dt>商品信息</dt>
				<dd><em><a href="/Ebuy/Category/skip?tz=productClass-add.jsp">新增</a></em><a href="/Ebuy/Category/query">分类管理</a></dd>
				<dd><em><a href="/Ebuy/Product/skip?tz=product-add.jsp">新增</a></em><a href="/Ebuy/Product/query">商品管理</a></dd>
				<dt>订单管理</dt>
				<dd><a href="/Ebuy/OrderManaServlet/osef">订单管理</a></dd>
				<dt>留言管理</dt>
				<dd><a href="/Ebuy/EnquiryManaServlet/seenqfind">留言管理</a></dd>
				<dt>新闻管理</dt>
				<dd><em><a href="/Ebuy/manage/news-add.jsp">新增</a></em><a href="/Ebuy/NewsManServlet/newfind">新闻管理</a></dd>
			</dl>
		</div>
	</div>
	<div class="main">
		<h2>添加分类</h2>
		<div class="manage">
			<form action="/Ebuy/Category/add">
				<table class="form">
					<tr>
						<td class="field">父分类：</td>
						<td>
							<select name="parentId">
								<option value="1">图书音像</option>
								<option value="2">百货</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="field">分类名称：</td>
						<td><input type="text" class="text" name="className" value="" /></td>
					</tr>
					<tr>
						<td></td>
						<td><label class="ui-blue"><input type="submit" name="submit" value="更新" /></label></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div id="footer">
	Copyright &copy; 2016 九云IT教育 All Rights Reserved. 京ICP证1000001号
</div>
</body>
</html>
