<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.dao.impl.ProductDaoimpl" %>
<%@ page import="java.net.CookieHandler"%>
<%@ page import="java.util.*" %>
<%@ page import="com.dao.impl.ProductCategoryDaoimpl" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>易买网 - 首页</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function.js"></script>

</head>
<body>
<div id="header" class="wrap">
	<div id="logo"><img src="../images/logo.gif" /></div>
	<%
	String name=(String) request.getSession().getAttribute("name");
	System.out.println("index:"+name);
	int usid=0;
	if(name!=null){
		usid=(Integer)request.getSession().getAttribute("userId");
	}
	%>
	<div class="help"><a href="#" onclick="gopd('<%=name %>',1)" class="shopping">购物车</a>
	<c:choose>
	<c:when test="<%=name==null %>">
			<a href="/Ebuy/frontend/login.jsp"> 登入</a>
			<a href="/Ebuy/frontend/register.jsp">注册</a>
	</c:when>
	<c:otherwise>
			<a href="#"><%=name %></a><a href="/Ebuy/LoginServlet/exit">退出</a>
	</c:otherwise>
	</c:choose>
	<a href="/Ebuy/GuestServlet/findse">留言</a>
	<a href="/Ebuy/frontend/BackstageMana.jsp">后台管理</a>
	</div>
	<div class="navbar">
		<ul class="clearfix">
			<li class="current"><a href="#">首页</a></li>
			<li><a href="#">图书</a></li>
			<li><a href="#">百货</a></li>
			<li><a href="#">品牌</a></li>
			<li><a href="#">促销</a></li>
		</ul>
	</div>
</div>
<div id="childNav">
	<div class="wrap">
	<%	
			//
			ProductCategoryDaoimpl prcat = new ProductCategoryDaoimpl();
			request.setAttribute("prcat", prcat.pagequery(1,12));
		%>
		<ul class="clearfix">
			<c:forEach items="${prcat }" var="ca">
				<li class="first"><a href="product-list.jsp?id=${ca.EPC_ID }">${ca.EPC_NAME }</a></li>
			</c:forEach>
		</ul>
	</div>
</div>
<div id="position" class="wrap">
	您现在的位置：<a href="index.jsp">易买网</a> &gt; 商品详情
</div>
<div id="main" class="wrap">
	<div class="lefter">
		<div class="box">
			<h2>商品分类</h2>
			<%
				ProductCategoryDaoimpl pl = new ProductCategoryDaoimpl();
			request.setAttribute("zlist1", pl.querylist(1));   //查询多个商品类信息（父类ID）
			request.setAttribute("zlist2", pl.querylist(2));
			%>
			<dl>
				<dt>图书音像</dt>
				<c:forEach items="${zlist1 }" var="kl">
					<dd><a href="product-list.jsp?id=${kl.EPC_ID }">${kl.EPC_NAME }</a></dd>
				</c:forEach>
				<dt>百货</dt>
				<c:forEach items="${zlist2 }" var="kl">
					<dd><a href="product-list.jsp?id=${kl.EPC_ID }">${kl.EPC_NAME }</a></dd>
				</c:forEach>
			</dl>
		</div>
	</div>
	<div id="product" class="main">
	<% 
			int id = Integer.parseInt(request.getParameter("id"));
			ProductDaoimpl p = new ProductDaoimpl();
			request.setAttribute("sp", p.query(id));
			
			//添加到最近浏览
			Cookie[] getGoodsCookie = request.getCookies();  //得到所有Cookie
			List<Cookie> clist = new ArrayList<Cookie>(); //保存所有的productLately:开头的Cookie
			Cookie tempCookie = null;  //用来保存Cookie
			if (getGoodsCookie.length>0 && getGoodsCookie != null) {
				for(Cookie c:getGoodsCookie){
					String cookieName = c.getName();
					/* System.out.println(cookieName); */
					if(cookieName.contains("productLately")){
						clist.add(c);
						String t = cookieName.substring(13);
						if(t.equals(id+"")){
							//System.out.println(":::::::::"+t);
							tempCookie = c;
						}
					}
				}
			}
			
			if(clist.size()>3 && tempCookie==null){
				tempCookie = clist.get(0);
			}
			//若在其中，则删除该Cookie
			if(tempCookie!=null){
				tempCookie.setMaxAge(0);
				response.addCookie(tempCookie);
			}
			
			//创建Cookie
			Cookie kie = new Cookie("productLately"+id,id+"");
			response.addCookie(kie);
		%>
		<h1>${sp.EP_NAME }</h1>
	
		<div class="infos">
			<br /><br />
			<div class="thumb"><img src="../images/product/${sp.EP_FILE_NAME }" width="250px" /></div>
			<div class="buy">
				<p>商城价：<span class="price">￥${sp.EP_PRICE }</span></p>
				<c:if test="${sp.EP_STOCK>0 }">
					<p>库　存：有货</p>
				</c:if>
				<c:if test="${sp.EP_STOCK<=0 }">
					<p>库　存：<span style="color:red;">缺货</span></p>
				</c:if>
				<br />
				<br />
				<br />
				<div class="button"><input type="button" name="button" value="" onclick="gopd('<%=name %>',2)" /><a href="#" onclick="gopd('<%=name %>',${sp.EP_ID })" >放入购物车</a></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="introduce">
			<h2><strong>商品详情</strong></h2>
			<div class="text">
				<p>${sp.EP_DESCRIPTION }</p>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div id="footer">
	Copyright &copy; 2016 九云IT教育 All Rights Reserved. 京ICP证1000001号
</div>
</body>
<script type="text/javascript">
//判断是否登入
function gopd(na,n){
	console.log(na,n);
	if(na=="null"){
		location.href="/Ebuy/frontend/login.jsp";
	}else{
		if(n==1){
			location.href="/Ebuy/Shopping/gfind";
		}else if(n==2){
			location.href="/Ebuy/PayServlet/fuk?je=${sp.EP_PRICE }&id=<%=usid%>";
		}else{
			location.href="/Ebuy/Shopping/join?id="+n;
		}
	}
}
</script>
</html>
