<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.dao.impl.ProductCategoryDaoimpl"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>易买网 - 首页</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function.js"></script>
<script type="text/javascript">
	function changcode() {
		document.getElementById("veryCode").src = document
				.getElementById("veryCode").src
				+ "?time=" + new Date().getTime();
	}
	
	// 判断是否登入
	function gopd(na){
		console.log(na);
		if(na=="null"){
			location.href="/Ebuy/frontend/login.jsp";
		}else{
			location.href="/Ebuy/Shopping/gfind";
		}
	}
</script>
</head>
<body>
	<div id="header" class="wrap">
		<div id="logo">
			<img src="../images/logo.gif" />
		</div>
		<div class="help">
		<%
			String name = (String) request.getSession().getAttribute("name");
			//System.out.println("index:" + name);
		%>
			<a href="#" onclick="gopd('<%=name %>')" class="shopping">购物车</a><a
				href="/Ebuy/frontend/login.jsp">登录</a><a
				href="/Ebuy/frontend/register.jsp">注册</a><a
				href="/Ebuy/GuestServlet/findse">留言</a>
				<a href="/Ebuy/frontend/BackstageMana.jsp">后台管理</a>
		</div>
		<div class="navbar">
			<ul class="clearfix">
				<li class="current"><a href="#">首页</a></li>
				<li><a href="#">图书</a></li>
				<li><a href="#">百货</a></li>
				<li><a href="#">品牌</a></li>
				<li><a href="#">促销</a></li>
			</ul>
		</div>
	</div>
	<div id="childNav">
		<div class="wrap">
			<%
				//
			ProductCategoryDaoimpl prcat = new ProductCategoryDaoimpl();
			request.setAttribute("prcat", prcat.pagequery(1, 12));
			%>
			<ul class="clearfix">
				<c:forEach items="${prcat }" var="ca">
					<li class="first"><a href="product-list.jsp?id=${ca.EPC_ID }">${ca.EPC_NAME }</a></li>
				</c:forEach>
			</ul>
		</div>
	</div>
	<div id="register" class="wrap">
		<div class="shadow">
			<em class="corner lb"></em> <em class="corner rt"></em>
			<div class="box">
				<h1>欢迎回到易买网</h1>
				<form id="loginForm" method="post" action="/Ebuy/LoginServlet/login"
					onsubmit="return checkForm(this)">
					<table>
						<tr>
							<td class="field">用户名：</td>
							<td><input class="text" type="text" name="userName"
								onfocus="FocusItem(this)" onblur="CheckItem(this);" /><span></span></td>
						</tr>
						<tr>
							<td class="field">登录密码：</td>
							<td><input class="text" type="password" id="passWord"
								name="passWord" onfocus="FocusItem(this)"
								onblur="CheckItem(this);" /><span></span></td>
						</tr>
						<tr>
							<td class="field">验证码：</td>
							<td><input class="text verycode" type="text" name="veryCode"
								onfocus="FocusItem(this)" onblur="CheckItem(this);" /><img
								id="veryCode" src="/Ebuy/LoginServlet/yzm" onclick="changcode()" /><span></span></td>
						</tr>
						<tr>
							<td></td>
							<td><label class="ui-green"><input type="submit"
									name="submit" value="立即登录" /></label></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div id="footer">Copyright &copy; 2016 九云IT教育 All Rights
		Reserved. 京ICP证1000001号</div>
</body>
</html>
