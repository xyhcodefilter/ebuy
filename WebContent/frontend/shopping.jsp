<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ page import="com.dao.impl.ProductCategoryDaoimpl" %>
         <%@ page import = "com.dao.impl.ShoppingDaoImpl" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>易买网 - 首页</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function.js"></script>
<script type="text/javascript">
//判断是否登入
function gopd(na){
	console.log(na);
	if(na=="null"){
		location.href="/Ebuy/frontend/login.jsp";
	}else{
		location.href="/Ebuy/Shopping/gfind";
	}
}
</script>
</head>
<body>
<div id="header" class="wrap">
	<div id="logo"><img src="../images/logo.gif" /></div>
	<%
	// 购物车dao
		ShoppingDaoImpl sh = new ShoppingDaoImpl();
	String name=(String) request.getSession().getAttribute("name");
	System.out.println("index:"+name);
	%>
	<div class="help"><a href="#" onclick="gopd('<%=name %>')" class="shopping">购物车</a>
	<c:choose>
	<c:when test="<%=name==null %>">
			<a href="/Ebuy/frontend/login.jsp">登入</a>
			<a href="/Ebuy/frontend/register.jsp">注册</a>
	</c:when>
	<c:otherwise>
			<a href="#"><%=name %></a><a href="/Ebuy/LoginServlet/exit">退出</a>
	</c:otherwise>
	</c:choose>
	<a href="/Ebuy/GuestServlet/findse">留言</a>
	<a href="/Ebuy/frontend/BackstageMana.jsp">后台管理</a>
	</div>
	
	<div class="navbar">
		<ul class="clearfix">
			<li class="current"><a href="#">首页</a></li>
			<li><a href="#">图书</a></li>
			<li><a href="#">百货</a></li>
			<li><a href="#">品牌</a></li>
			<li><a href="#">促销</a></li>
		</ul>
	</div>
</div>
<div id="childNav">
	<div class="wrap">
	<%	
			//
			ProductCategoryDaoimpl prcat = new ProductCategoryDaoimpl();
			request.setAttribute("prcat", prcat.pagequery(1,12));
		%>
		<ul class="clearfix">
			<c:forEach items="${prcat }" var="ca">
				<li class="first"><a href="/Ebuy/frontend/product-list.jsp?id=${ca.EPC_ID }">${ca.EPC_NAME }</a></li>
			</c:forEach>
		</ul>
	</div>
</div>
<div id="position" class="wrap">
	您现在的位置：<a href="/Ebuy/frontend/index.jsp">易买网</a> &gt; 购物车
</div>
<div class="wrap">
	<div id="shopping">
			<table>
				<tr>
					<th>商品名称</th>
					<th>商品价格</th>
					<th>购买数量</th>
					<th>操作</th>
				</tr>
				<c:if test="${list.size()!=0 }">
					<c:forEach begin="${0 }" end="${list.size()-1 }" var="k">
						<tr id="product_id_${list.get(k).EOD_ID }">
							<td class="thumb"><img src="../images/product/${list2.get(k).EP_FILE_NAME }" width="80px" height="80px" /><a href="product-view.jsp">${list2.get(k).EP_NAME }</a></td>
							<td class="price" id="price_id_${list.get(k).EOD_ID }">
								￥<span class="jg">${list.get(k).EOD_COST }</span>
								<input type="hidden" value="${list2.get(k).EP_PRICE }" />
							</td>
							<td class="number">
								<dl>
									<dd style="width:20px;" onclick="reloadPrice(${list.get(k).EOD_ID },false);">-</dd>
									<dt style="margin-top:4px;" >
										<input id="number_id_${list.get(k).EOD_ID }" type="text" name="number" value="${list.get(k).EOD_QUANTITY }" onblur="loseFocus(${list.get(k).EOD_ID })" onkeyup="value=value.replace(/[^\d]/g,'')" maxlength="3" style="outline:none;" />
									</dt>
									<dd style="width:20px;" onclick="reloadPrice(${list.get(k).EOD_ID },true);">+</dd>
								</dl>
							</td>
							<td class="delete"><a href="javascript:delShopping(${list.get(k).EOD_ID });">删除</a></td>
						</tr>
					</c:forEach>
				</c:if>
			</table>
			<div class="button" style="margin-top:40px;">
				<span style="font-weight:bold; font-size:16px;">
					合计：<span style="color:#c00">￥</span><span id="total" style="color:#c00;font-weight:bold; font-size:16px;">${total }</span>
				</span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="submit" value="" onclick="tjiao()"/>
			</div>
	</div>
	
</div>
<div id="footer">
	Copyright &copy; 2016 九云IT教育 All Rights Reserved. 京ICP证1000001号
</div>
</body>
<script>
	//失去焦点
	function loseFocus(id){
		var price = document.getElementById("price_id_" + id).getElementsByTagName("input")[0].value;
		var priceBox = document.getElementById("price_id_" + id).getElementsByTagName("span")[0];
		var number = document.getElementById("number_id_" + id);
		//alert(number.value)
		if(number.value<1){
			number.value=1;
		}
		priceBox.innerHTML = price * number.value;
		
		var jg = document.querySelectorAll(".jg");  //得到每个价格
		var total = document.querySelector("#total"); 
		var num = 0;
		for(var i=0;i<jg.length;i++){
			num+=parseFloat(jg[i].innerText);
		}
		//console.log(num);
		total.innerHTML= num
	}
	
	function tjiao(){
		var total = document.querySelector("#total").innerHTML;
		if(total>0){
			location.href="/Ebuy/PayServlet/fuk?je="+total;
		}else{
			alert("你的购物车居然没有商品！");
		}
	}
</script>
</html>
