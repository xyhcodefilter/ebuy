<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.dao.impl.ProductCategoryDaoimpl"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>易买网 - 首页</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function.js"></script>
<script type="text/javascript">
// 判断是否登入
function gopd(na){
	console.log(na);
	if(na=="null"){
		location.href="/Ebuy/frontend/login.jsp";
	}else{
		location.href="/Ebuy/Shopping/gfind";
	}
}
</script>
</head>
<body>

	<div id="header" class="wrap">
		<div id="logo">
			<img src="../images/logo.gif" />
		</div>
		<%
			String name = (String) request.getSession().getAttribute("name");
		System.out.println("index:" + name);
		%>
		<div class="help">
			<a href="#" onclick="gopd('<%=name %>')" class="shopping">购物车</a>
			<c:choose>
				<c:when test="<%=name == null%>">
					<a href="/Ebuy/frontend/login.jsp">登入</a>
					<a href="/Ebuy/frontend/register.jsp">注册</a>
				</c:when>
				<c:otherwise>
					<a href="#"><%=name%></a>
					<a href="/Ebuy/LoginServlet/exit">退出</a>
				</c:otherwise>
			</c:choose>
			<a href="/Ebuy/GuestServlet/findse">留言</a>
			<a href="/Ebuy/frontend/BackstageMana.jsp">后台管理</a>
		</div>
		<div class="navbar">
			<ul class="clearfix">
				<li class="current"><a href="#">首页</a></li>
				<li><a href="#">图书</a></li>
				<li><a href="#">百货</a></li>
				<li><a href="#">品牌</a></li>
				<li><a href="#">促销</a></li>
			</ul>
		</div>
	</div>
	<div id="childNav">
		<div class="wrap">
			<%
				//
			ProductCategoryDaoimpl prcat = new ProductCategoryDaoimpl();
			request.setAttribute("prcat", prcat.pagequery(1, 12));
			%>
			<ul class="clearfix">
				<c:forEach items="${prcat }" var="ca">
					<li class="first"><a href="product-list.jsp?id=${ca.EPC_ID }">${ca.EPC_NAME }</a></li>
				</c:forEach>
			</ul>
		</div>
	</div>
	<div id="position" class="wrap">
		您现在的位置：<a href="/Ebuy/frontend/index.jsp">易买网</a> &gt; 阅读新闻
	</div>
	<div id="main" class="wrap">
		<div class="left-side">
			<div class="news-list">
				<h4>最新公告</h4>
				<ul>
					<c:forEach items="${seall }" var="k">
						<li><a href="/Ebuy/FirstPageServlet/onnews?id=${k.EN_ID }"
							target="_blank">${k.EN_TITLE }</a></li>
					</c:forEach>
				</ul>
			</div>
			<div class="spacer"></div>
			<div class="news-list">
				<h4>新闻动态</h4>
				<ul>
					<c:forEach items="${oldne }" var="k">
						<li><a href="/Ebuy/FirstPageServlet/onnews?id=${k.EN_ID }"
							target="_blank">${k.EN_TITLE }</a></li>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div id="news" class="right-main">
			<c:forEach items="${seid }" var="k">
				<h1>${k.EN_TITLE}</h1>
				<div class="content">${k.EN_CONTENT }</div>
			</c:forEach>

		</div>
		<div class="clear"></div>
	</div>
	<div id="footer">Copyright &copy; 2016 九云IT教育 All Rights
		Reserved. 京ICP证1000001号</div>
</body>
</html>
