<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.dao.impl.ProductCategoryDaoimpl" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>易买网 - 首页</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function.js"></script>
<script type="text/javascript">
function changcode(){
	document.getElementById("veryCode").src=document.getElementById("veryCode").src+"?time="+new Date().getTime();
}

// 判断是否登入
function gopd(na){
	console.log(na);
	if(na=="null"){
		location.href="/Ebuy/frontend/login.jsp";
	}else{
		location.href="/Ebuy/Shopping/gfind";
	}
}
</script>
</head>
<body>
<div id="header" class="wrap">
	<div id="logo"><img src="../images/logo.gif" /></div>
	<%
	String name=(String) request.getSession().getAttribute("name");
	System.out.println("index:"+name);
	%>
	<div class="help"><a href="#" onclick="gopd('<%=name %>')" class="shopping">购物车</a>
	<c:choose>
	<c:when test="<%=name==null %>">
			<a href="/Ebuy/frontend/login.jsp">登入</a>
			<a href="/Ebuy/frontend/register.jsp">注册</a>
	</c:when>
	<c:otherwise>
			<a href="#"><%=name %></a><a href="/Ebuy/LoginServlet/exit">退出</a>
	</c:otherwise>
	</c:choose>
	<a href="/Ebuy/GuestServlet/findse">留言</a>
	<a href="/Ebuy/frontend/BackstageMana.jsp">后台管理</a>
	</div>
	
	<div class="navbar">
		<ul class="clearfix">
			<li class="current"><a href="/Ebuyfrontend/index.jsp">首页</a></li>
			<li><a href="#">图书</a></li>
			<li><a href="#">百货</a></li>
			<li><a href="#">品牌</a></li>
			<li><a href="#">促销</a></li>
		</ul>
	</div>
</div>
<div id="childNav">
	<div class="wrap">
	<%	
			//
			ProductCategoryDaoimpl prcat = new ProductCategoryDaoimpl();
			request.setAttribute("prcat", prcat.pagequery(1,12));
		%>
		<ul class="clearfix">
			<c:forEach items="${prcat }" var="ca">
				<li class="first"><a href="product-list.jsp?id=${ca.EPC_ID }">${ca.EPC_NAME }</a></li>
			</c:forEach>
		</ul>
	</div>
</div>
<div id="register" class="wrap">
	<div class="shadow">
		<em class="corner lb"></em>
		<em class="corner rt"></em>
		<div class="box">
			<h1>欢迎注册易买网</h1>
			<ul class="steps clearfix">
				<li class="current"><em></em>填写注册信息</li>
				<li class="last"><em></em>注册成功</li>
			</ul>
			<form id="regForm" method="post" action="/Ebuy/LoginServlet/reg" onsubmit="return checkForm(this);">
				<table>
					<tr>
						<td class="field">用户名：</td>
						<td><input class="text" type="text" name="userAc" onfocus="FocusItem(this)" onblur="CheckItem(this);" /><span></span></td>
					</tr>
					<tr>
						<td class="field">姓名：</td>
						<td><input class="text" type="text" name="userName" onfocus="FocusItem(this)" onblur="CheckItem(this);" /><span></span></td>
					</tr>
					<tr>
						<td class="field">性别：</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="userSex" value="男" checked="checked" />男&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" value="女" name="userSex" />女<span></span></td>
					</tr>
					<tr>
						<td class="field">出生日期：</td>
						<td><input type="date" name="userBirthday" onfocus="FocusItem(this)" onblur="CheckItem(this);"/></td>
					</tr>
					<tr>
						<td class="field">身份证号：</td>
						<td><input class="text" type="tel" maxlength="18" name="userIdentity" onfocus="FocusItem(this)" onblur="CheckItem(this);"/><span></span></td>
					</tr>
					<tr>
						<td class="field">邮箱：</td>
						<td><input class="text" type="email" name="userEmail" onfocus="FocusItem(this)" onblur="CheckItem(this);"/><span></span></td>
					</tr>
					<tr>
						<td class="field">电话号码：</td>
						<td><input class="text" type="tel" name="userMobile" maxlength="11" onfocus="FocusItem(this)" onblur="CheckItem(this);"/><span></span></td>
					</tr>
					<tr>
						<td class="field">家庭住址：</td>
						<td><input class="text" type="text" name="userAddress" onfocus="FocusItem(this)" onblur="CheckItem(this);"/><span></span></td>
					</tr>
					<tr>
						<td class="field">注册类型：</td>
						<td>
							<select name="userStatus">
								<option value='1'>用户</option>
								<option value='2'>管理员</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="field">登录密码：</td>
						<td><input class="text" type="password" id="passWord" name="passWord" onfocus="FocusItem(this)" onblur="CheckItem(this);" /><span></span></td>
					</tr>
					<tr>
						<td class="field">确认密码：</td>
						<td><input class="text" type="password" name="rePassWord" onfocus="FocusItem(this)" onblur="CheckItem(this);" /><span></span></td>
					</tr>
					<tr>
						<td class="field">验证码：</td>
						<td><input class="text verycode" type="text" name="veryCode"  onfocus="FocusItem(this)" onblur="CheckItem(this);" /><img id="veryCode" src="/Ebuy/LoginServlet/yzm" onclick="changcode()" /><span></span></td>
					</tr>
					<tr>
						<td></td>
						<td><label class="ui-green"><input type="submit" name="submit" value="提交注册" /></label></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div id="footer">
	Copyright &copy; 2016 九云IT教育 All Rights Reserved. 京ICP证1000001号
</div>
</body>
</html>
