<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.entity.Product"%>
<%@ page import="com.dao.impl.ProductDaoimpl"%>
<%@ page import="com.entity.PageBean"%>
<%@ page import="java.util.*"%>
<%@ page import="com.dao.impl.ProductCategoryDaoimpl"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>易买网 - 首页</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function.js"></script>
<script type="text/javascript">
// 判断是否登入
function gopd(na){
	console.log(na);
	if(na=="null"){
		location.href="/Ebuy/frontend/login.jsp";
	}else{
		location.href="/Ebuy/Shopping/gfind";
	}
}
</script>
<style>
#a1 {
	background-color: #4E6EF2;
	color: #FFFFFF;
}

#back, #nextpage {
	padding: 5px 5px;
	text-decoration: none;
	font-size: 14px;
	border-radius: 5%;
	color: #3951B3;
	background-color: #f5f5f5;
}
</style>
</head>
<body>
	<div id="header" class="wrap">
		<div id="logo">
			<img src="../images/logo.gif" />
		</div>
		<%
			String name = (String) request.getSession().getAttribute("name");
		System.out.println("index:" + name);
		%>
		<div class="help">
			<a href="#" onclick="gopd('<%=name %>')" class="shopping">购物车</a>
			<c:choose>
				<c:when test="<%=name == null%>">
					<a href="/Ebuy/frontend/login.jsp">登入</a>
					<a href="/Ebuy/frontend/register.jsp">注册</a>
				</c:when>
				<c:otherwise>
					<a href="#"><%=name%></a>
					<a href="/Ebuy/LoginServlet/exit">退出</a>
				</c:otherwise>
			</c:choose>
			<a href="/Ebuy/GuestServlet/findse">留言</a>
			<a href="/Ebuy/frontend/BackstageMana.jsp">后台管理</a>
		</div>
		<div class="navbar">
			<ul class="clearfix">
				<li class="current"><a href="#">首页</a></li>
				<li><a href="#">图书</a></li>
				<li><a href="#">百货</a></li>
				<li><a href="#">品牌</a></li>
				<li><a href="#">促销</a></li>
			</ul>
		</div>
	</div>
	<div id="childNav">
		<div class="wrap">
			<%
				//
			ProductCategoryDaoimpl prcat = new ProductCategoryDaoimpl();
			request.setAttribute("prcat", prcat.pagequery(1, 12));
			%>
			<ul class="clearfix">
				<c:forEach items="${prcat }" var="ca">
					<li class="first"><a href="product-list.jsp?id=${ca.EPC_ID }">${ca.EPC_NAME }</a></li>
				</c:forEach>
			</ul>
		</div>
	</div>
	<div id="position" class="wrap">
		您现在的位置：<a href="index.jsp">易买网</a> &gt; 商品分类
	</div>
	<div id="main" class="wrap">
		<div class="lefter">
			<div class="box">
				<h2>商品分类</h2>
				<%
					ProductDaoimpl produ = new ProductDaoimpl();
				ProductCategoryDaoimpl pl = new ProductCategoryDaoimpl();
				request.setAttribute("zlist1", pl.querylist(1)); //查询多个商品类信息（父类ID）
				request.setAttribute("zlist2", pl.querylist(2));

				//显示最近浏览
				Cookie[] getCookie = request.getCookies();
				List<Product> spList = new ArrayList<Product>();
				if (getCookie != null && getCookie.length > 0) {
					for (Cookie c : getCookie) {
						String cookieName = c.getName();
						if (cookieName.contains("productLately")) {
					spList.add(produ.query(Integer.parseInt(c.getValue())));
						}
					}
				}
				//降序
				Product[] prList = new Product[spList.size()];
				int k = 0;
				for (int i = prList.length - 1; i >= 0; i--) {
					//System.out.println(i);
					prList[k] = spList.get(i);
					k++;
				}

				request.setAttribute("prList", prList);
				%>
				<dl>
					<dt>图书音像</dt>
					<c:forEach items="${zlist1 }" var="kl">
						<dd>
							<a href="product-list.jsp?id=${kl.EPC_ID }">${kl.EPC_NAME }</a>
						</dd>
					</c:forEach>
					<dt>百货</dt>
					<c:forEach items="${zlist2 }" var="kl">
						<dd>
							<a href="product-list.jsp?id=${kl.EPC_ID }">${kl.EPC_NAME }</a>
						</dd>
					</c:forEach>
				</dl>
			</div>
			<div class="spacer"></div>
			<div class="last-view">
				<h2>最近浏览</h2>
				<dl class="clearfix">
					<c:forEach items="${prList }" var="sp">
						<dt>
							<a href="product-view.jsp?id=${sp.EP_ID }"><img
								src="../images/product/${sp.EP_FILE_NAME }" width="55px" /></a>
						</dt>
						<dd>
							<a href="product-view.jsp?id=${sp.EP_ID }">${sp.EP_NAME}</a>
						</dd>
					</c:forEach>
				</dl>
				<!-- <script type="text/javascript">
				document.write("Cookie中记录的购物车商品ID："+ getCookie("product") + "，可以在动态页面中进行读取");
			</script> -->
			</div>
		</div>
		<div class="main">
			<div class="product-list">
				<%
					String p = request.getParameter("p");

				int intp = 1;
				if (p != null) {
					intp = Integer.parseInt(p);
				}
				int id = Integer.parseInt(request.getParameter("id"));
				request.setAttribute("zs", pl.querylist(id));
				ProductDaoimpl ps = new ProductDaoimpl();
				int coun = ps.count(id);
				PageBean pagebean = new PageBean();
				pagebean.setSize(8);
				pagebean.setTotal(coun);
				pagebean.setCurpage(intp);

				List list = ps.fquery(intp, id, 8); //查询商品类多个商品信息
				request.setAttribute("pagdata", list);// 请求里面放数据
				request.setAttribute("pagebean", pagebean);// 把总页数request
				%>
				<h2>全部商品</h2>
				<div class="clear"></div>
				<ul class="product clearfix">
					<c:forEach items="${pagdata }" var="le1">
						<li>
							<dl>
								<dt>
									<a href="product-view.jsp?id=${le1.EP_ID }" target="_blank"><img
										src="../images/product/${le1.EP_FILE_NAME }" /></a>
								</dt>
								<dd class="title" style="text-align:center;margin-top:5px">
									<a href="product-view.jsp?id=${le1.EP_ID }" target="_blank">${le1.EP_NAME }</a>
								</dd>
								<dd class="price" style="text-align:center">￥${le1.EP_PRICE }</dd>
							</dl>
						</li>
					</c:forEach>
				</ul>
				<div class="clear"></div>
				<div class="pager">
					<ul class="clearfix">
						<c:if test="${pagebean.curpage>1 }">
							<li><a href="product-list.jsp?id=${param.id }&p=1">首页</a></li>
							<li><a
								href="product-list.jsp?id=${param.id }&p=${pagebean.curpage-1 }">上一页</a></li>
						</c:if>
						<c:forEach begin="${pagebean.begin}" end="${pagebean.end}" var="k">
							<c:set var="p" value="${k==pagebean.curpage?'a1':''}"></c:set>
							<li id="${p }"><a class="a1"
								href='product-list.jsp?id=${param.id }&p=${k }'>${k}</a></li>
						</c:forEach>
						<c:if test="${pagebean.curpage<pagebean.page }">
							<li><a
								href="product-list.jsp?id=${param.id }&p=${pagebean.curpage+1 }">下一页</a></li>
							<li><a
								href="product-list.jsp?id=${param.id }&p=${pagebean.page}">尾页</a></li>
						</c:if>
					</ul>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div id="footer">Copyright &copy; 2016 九云IT教育 All Rights
		Reserved. 京ICP证1000001号</div>
</body>
</html>
