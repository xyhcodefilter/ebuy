<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.dao.impl.ProductCategoryDaoimpl"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!--前台留言-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>易买网 - 首页</title>
<link type="text/css" rel="stylesheet" href="../css/style.css" />
<script type="text/javascript" src="../scripts/function.js"></script>
<script type="text/javascript">
//判断是否登入
function gopd(na){
	console.log(na);
	if(na=="null"){
		location.href="/Ebuy/frontend/login.jsp";
	}else{
		location.href="/Ebuy/Shopping/gfind";
	}
}
</script>
<style>
#a1 {
	background-color: #4E6EF2;
	color: #FFFFFF;  
}

#back, #nextpage {
	padding: 5px 5px;
	text-decoration: none;
	font-size: 14px;
	border-radius: 5%;
	color: #3951B3;
	background-color: #f5f5f5;
}
</style>
</head>
<body>
	<div id="header" class="wrap">
		<div id="logo">
			<img src="../images/logo.gif" />
		</div>
		<%
			String name = (String) request.getSession().getAttribute("name");
		System.out.println("index:" + name);
		%>
		<div class="help">
			<a href="#" onclick="gopd('<%=name %>')" class="shopping">购物车</a>
			<c:choose>
				<c:when test="<%=name == null%>">
					<a href="/Ebuy/frontend/login.jsp">登入</a>
					<a href="/Ebuy/frontend/register.jsp">注册</a>
				</c:when>
				<c:otherwise>
					<a href="#"><%=name%></a>
					<a href="/Ebuy/LoginServlet/exit">退出</a>
				</c:otherwise>
			</c:choose>
			<a href="/Ebuy/GuestServlet/findse">留言</a>
			<a href="/Ebuy/frontend/BackstageMana.jsp">后台管理</a>
		</div>
		<div class="navbar">
			<ul class="clearfix">
				<li class="current"><a href="#">首页</a></li>
				<li><a href="#">图书</a></li>
				<li><a href="#">百货</a></li>
				<li><a href="#">品牌</a></li>
				<li><a href="#">促销</a></li>
			</ul>
		</div>
	</div>
	<div id="childNav">
		<div class="wrap">
			<%
				//
			ProductCategoryDaoimpl prcat = new ProductCategoryDaoimpl();
			request.setAttribute("prcat", prcat.pagequery(1, 12));
			%>
			<ul class="clearfix">
				<c:forEach items="${prcat }" var="ca">
					<li class="first"><a
						href="/Ebuy/frontend/product-list.jsp?id=${ca.EPC_ID }">${ca.EPC_NAME }</a></li>
				</c:forEach>
			</ul>
		</div>
	</div>
	<div id="position" class="wrap">
		您现在的位置：<a href="/Ebuy/frontend/index.jsp">易买网</a> &gt; 在线留言
	</div>
	<div id="main" class="wrap">
		<div class="lefter">
			<div class="box">
				<h2>商品分类</h2>
				<%
					ProductCategoryDaoimpl pl = new ProductCategoryDaoimpl();
				request.setAttribute("zlist1", pl.querylist(1)); //查询多个商品类信息（父类ID）
				request.setAttribute("zlist2", pl.querylist(2));
				%>
				<dl>
					<dt>图书音像</dt>
					<c:forEach items="${zlist1 }" var="kl">
						<dd>
							<a href="/Ebuy/frontend/product-list.jsp?id=${kl.EPC_ID }">${kl.EPC_NAME }</a>
						</dd>
					</c:forEach>
					<dt>百货</dt>
					<c:forEach items="${zlist2 }" var="kl">
						<dd>
							<a href="/Ebuy/frontend/product-list.jsp?id=${kl.EPC_ID }">${kl.EPC_NAME }</a>
						</dd>
					</c:forEach>
				</dl>
			</div>
		</div>
		<div class="main">
			<div class="guestbook">
				<h2>全部留言</h2>
				<ul>
					<c:forEach items="${pagdata }" var="k">
						<li>
							<dl>
								<dt>${k.EC_CONTENT }</dt>
								<dd class="author">
									网友：${k.EC_NICK_NAME } <span class="timer">${k.EC_REPLY_TIME }</span>
								</dd>
								<dd>${k.EC_REPLY }</dd>
							</dl>
						</li>
					</c:forEach>
				</ul>
				<div class="clear"></div>
				<div class="pager">
					<ul class="clearfix">
						<c:if test="${pagebean.curpage>1 }">
							<li><a href="/Ebuy/GuestServlet/findse?p=1">首页</a></li>
							<li><a
								href="/Ebuy/GuestServlet/findse?p=${pagebean.curpage-1 }">上一页</a></li>
						</c:if>
						<c:forEach begin="${pagebean.begin}" end="${pagebean.end}" var="k">
							<c:set var="p" value="${k==pagebean.curpage?'a1':''}"></c:set>
							<li id="${p }"><a class="a1"
								href='/Ebuy/GuestServlet/findse?p=${k }'>${k}</a></li>
						</c:forEach>
						<c:if test="${pagebean.curpage<pagebean.page }">
							<li><a
								href="/Ebuy/GuestServlet/findse?p=${pagebean.curpage+1 }">下一页</a></li>
							<li><a href="/Ebuy/GuestServlet/findse?p=${pagebean.page}">尾页</a></li>
						</c:if>
					</ul>
				</div>
				<div id="reply-box">
					<form method="post" action="/Ebuy/GuestServlet/addComment">
						<table>
							<tr>
								<td class="field">昵称：</td>
								<td><input class="text" type="text" name="guestName"
									value="${flo }" ${cf } /></td>
							</tr>
							<tr>
								<td class="field">留言标题：</td>
								<td><input class="text" type="text" name="guestTitle" /></td>
							</tr>
							<tr>
								<td class="field">留言内容：</td>
								<td><textarea name="guestContent"></textarea></td>
							</tr>
							<tr>
								<td></td>
								<td><label class="ui-blue"><input type="submit"
										name="submit" value="提交留言" /></label></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div id="footer">Copyright &copy; 2016 九云IT教育 All Rights
		Reserved. 京ICP证1000001号</div>
</body>
</html>
