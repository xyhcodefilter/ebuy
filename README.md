# Ebuy

#### 介绍
商城项目

前台

![输入图片说明](https://images.gitee.com/uploads/images/2021/0624/163821_88e10711_7956133.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0624/164007_ffa0d8c4_7956133.png "屏幕截图.png")

后台
![输入图片说明](https://images.gitee.com/uploads/images/2021/0624/164020_a957b642_7956133.png "屏幕截图.png")

#### 软件架构
MVC  Java JSP JavaScript MYSQL


#### 安装教程

把项目下载后 需要在本地新建一个数据库:ebuy    把数据库文件夹的数据导入ebuy下 ，

#### 使用说明

1.  需要更改数据库JDBCTool类的数据库名 数据库账号 数据库密码
2.  修改AlipayConfig类 配置自己的支付宝开放平台信息

