package com.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCTool {
	
	//数据库连接
	static String dbname="root"; //账号
	static String dbpass="root";//密码
	static String url="jdbc:mysql://localhost:3306/ebuy";
	static String driver="com.mysql.jdbc.Driver";
	static Connection conn=null;
	static Statement sta=null;
	static ResultSet rs= null;
	
	public static Connection openconn() {
		try {
			Class.forName(driver);
			try {
				conn=DriverManager.getConnection(url,dbname,dbpass);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
}
