package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.impl.ProductCategoryDaoimpl;
import com.entity.PageBean;
// 后台分类
@WebServlet("/Category/*")
public class CategoryManaServlet extends BaseServlet {
	
	ProductCategoryDaoimpl pr = new ProductCategoryDaoimpl();
	
	//添加
	protected void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("add");
		String type = request.getParameter("className");
		int fid = Integer.parseInt(request.getParameter("parentId"));
		//System.out.println(type);
		int s = pr.add(type,fid);
		request.setAttribute("url", "/Ebuy/Category/skip?tz=productClass-add.jsp");
		request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
	}
	
	//修改
	protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("update");
		String name = request.getParameter("className");
		int fid = Integer.parseInt(request.getParameter("parentId"));
		int jid = Integer.parseInt(request.getParameter("jid"));
		//System.out.println(fid+":"+name+":"+jid);
		int s = pr.update(jid,name,fid);
		request.setAttribute("url", "/Ebuy/Category/query");
		request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
	}
	
	//删除
	protected void del(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		int id = Integer.parseInt(request.getParameter("id"));
		List list=pr.secate(id);
		
		if(list.size()==0) {
			System.out.println(list);
			int s = pr.del(id);
			response.sendRedirect("/Ebuy/Category/query");
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('该类有其他商品 请先移除！');location.href='/Ebuy/Category/query'</script>");
		}
		
	}
	
	//查询
	protected void query(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("query");
		String p = request.getParameter("p");

		int intp = 1;
		if (p != null) {
			intp = Integer.parseInt(p);
		}
		int coun = pr.count();
		PageBean pagebean = new PageBean();
		pagebean.setSize(6);
		pagebean.setTotal(coun);
		pagebean.setCurpage(intp);
		List list = pr.pagequery(intp,6);
		request.setAttribute("pagdata", list);// 请求里面放数据
		request.setAttribute("pagebean", pagebean);// 把总页数request
		request.getRequestDispatcher("/manage/productClass.jsp").forward(request, response);
	}
	
	//跳转
	protected void skip(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("skip");
		String tz = request.getParameter("tz");
		request.getRequestDispatcher("/manage/"+tz).forward(request, response);
	}
}
