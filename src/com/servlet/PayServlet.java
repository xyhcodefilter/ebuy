package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PayServlet
 */
@WebServlet("/PayServlet/*")
public class PayServlet extends BaseServlet {
	//支付
	protected void fuk(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String jd=request.getParameter("je");
		request.setAttribute("zje", jd);
		request.getRequestDispatcher("/frontend/PaymentOrder.jsp").forward(request, response);
	}

}
