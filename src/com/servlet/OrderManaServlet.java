package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.impl.OrderManaDaoimpl;
import com.entity.Order;
import com.entity.PageBean;
// 订单后台
/**
 * Servlet implementation class OrderManaServlet
 */
@WebServlet("/OrderManaServlet/*")
public class OrderManaServlet extends BaseServlet {
	OrderManaDaoimpl orimp=new OrderManaDaoimpl();
	
	//分页查询订单
	protected void osef(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		String p = request.getParameter("p");
		int intp = 1;
		if (p != null) {
			intp = Integer.parseInt(p);
		}
		int orcun=orimp.couorall();
		request.setAttribute("url","/Ebuy/OrderManaServlet/osef?p=");
		PageBean orma=new PageBean();
		orma.setSize(5);
		orma.setTotal(orcun);
		orma.setCurpage(intp);
		List orlis = orimp.orall(intp);
		request.setAttribute("orlis", orlis);
		request.setAttribute("pagebean", orma);
		request.getRequestDispatcher("/manage/order.jsp").forward(request, response);
	}
	
	//删除订单
	protected void orde(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		String id=request.getParameter("orid");
		int oid=Integer.parseInt(id);
		int ordu=orimp.deor(oid);
		if(ordu>0) {
			response.sendRedirect("/Ebuy/OrderManaServlet/osef");
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('删除失败！');location.href='/Ebuy/OrderManaServlet/osef'</script>");
		}
	}
	
	//通过ID查订单
	protected void seorid(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		String orid=request.getParameter("oid");
		int orsid=Integer.parseInt(orid);
		List list=orimp.orconfig(orsid);
		request.setAttribute("orlist", list);
		request.getRequestDispatcher("/manage/order-modify.jsp").forward(request, response);
	}
	
	//更新订单
	protected void uporder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String id=request.getParameter("orderId");
		int oid=Integer.parseInt(id);
		String na=request.getParameter("name");
		String add=request.getParameter("addre");
		String je=request.getParameter("AmountOf");
		float Amt=Float.valueOf(je);
		String time=request.getParameter("Orderstime");
		String sta=request.getParameter("State");
		int st=Integer.parseInt(sta);
		Order or=new Order(oid,na,add,Amt,time,st);
		int up=orimp.upor(or);
		if(up>0) {
			request.setAttribute("url", "/Ebuy/OrderManaServlet/osef");
			request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('修改失败！');location.href='/Ebuy/manage/order-modify.jsp'</script>");
		}
	}
	
	//搜索分页
	protected void sef(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String oid=request.getParameter("orderId");
		String name=request.getParameter("userName");
		if(oid.equals("") && name.equals("")) {
			response.sendRedirect("/Ebuy/OrderManaServlet/osef");
		}
		
		if(oid!=null && name.equals("")) {
			int orid=Integer.parseInt(oid);
			String p = request.getParameter("p");
			int intp = 1;
			if (p != null) {
				intp = Integer.parseInt(p);
			}
			int orcun=orimp.secoid(orid);
			request.setAttribute("url", "/Ebuy/OrderManaServlet/sef?userName="+name+"&orderId="+oid+"&p=");
			PageBean orma=new PageBean();
			orma.setSize(5);
			orma.setTotal(orcun);
			orma.setCurpage(intp);
			List seidnal = orimp.sorid(intp, orid);
			request.setAttribute("orlis", seidnal);
			request.setAttribute("pagebean", orma);
			request.getRequestDispatcher("/manage/order.jsp").forward(request, response);
		}
		
		if(oid.equals("") && name!=null) {
			String p = request.getParameter("p");
			int intp = 1;
			if (p != null) {
				intp = Integer.parseInt(p);
			}
			int orcun=orimp.secona(name);
			request.setAttribute("url", "/Ebuy/OrderManaServlet/sef?userName="+name+"&orderId="+oid+"&p=");
			PageBean orma=new PageBean();
			orma.setSize(5);
			orma.setTotal(orcun);
			orma.setCurpage(intp);
			List seidnal = orimp.sornam(intp, name);
			request.setAttribute("orlis", seidnal);
			request.setAttribute("pagebean", orma);
			request.getRequestDispatcher("/manage/order.jsp").forward(request, response);
		}
		
		if(oid !=null && name !=null) {
			int orid=Integer.parseInt(oid);
			String p = request.getParameter("p");
			int intp = 1;
			if (p != null) {
				intp = Integer.parseInt(p);
			}
			int orcun=orimp.secoina(orid, name);
			request.setAttribute("url", "/Ebuy/OrderManaServlet/sef?userName="+name+"&orderId="+oid+"&p=");
			PageBean orma=new PageBean();
			orma.setSize(5);
			orma.setTotal(orcun);
			orma.setCurpage(intp);
			List seidnal = orimp.soridna(intp, orid, name);
			request.setAttribute("orlis", seidnal);
			request.setAttribute("pagebean", orma);
			request.getRequestDispatcher("/manage/order.jsp").forward(request, response);
		}
	}
}
