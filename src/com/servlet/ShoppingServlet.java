package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.impl.ProductDaoimpl;
import com.dao.impl.ShoppingDaoImpl;
import com.entity.Product;
import com.entity.Shopping;

/**
 * Servlet implementation class ShoppingServlet
 */
@WebServlet("/Shopping/*")
public class ShoppingServlet extends BaseServlet {
	
	ShoppingDaoImpl sh = new ShoppingDaoImpl();
	ProductDaoimpl pr = new ProductDaoimpl();
	
	// 查询购物车(用户id，1:未下单，2:已下单)
	protected void gfind(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("gfind");
		//得到用户id
		HttpSession ssion = request.getSession();
		int uid = (Integer) ssion.getAttribute("userId");
		//System.out.println("uid:"+uid);
		List list = sh.gfind(uid, 1);   //购物车表数据
		if(list.size()==0) {
			request.setAttribute("list", list);
			request.setAttribute("total", 0.0);
			request.getRequestDispatcher("/frontend/shopping.jsp").forward(request, response);
		}
		List<Product> list2 = new ArrayList<Product>();  //商品表数据
		for(int i=0;i<list.size();i++) {
			Shopping sing = (Shopping)list.get(i);
			list2.add(pr.query(sing.getEP_ID()));
		}
		float total = sh.getTotal(uid);  //合计
		//System.out.println(list2);
		request.setAttribute("list", list);
		request.setAttribute("list2", list2);
		request.setAttribute("total", total);
		request.getRequestDispatcher("/frontend/shopping.jsp").forward(request, response);
	}
	
	//合计(用户id)
	protected void getTotal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("getTotal");
		
	}
	
	//加入购物车
	protected void join(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		//System.out.println("join");
		//得到商品id
		int id = Integer.parseInt(request.getParameter("id"));
		//System.out.println("id:"+id);
		//得到用户id
		HttpSession ssion = request.getSession();
		int uid = (Integer) ssion.getAttribute("userId");
		//System.out.println("uid:"+uid);
		Product sp = pr.query(id);
		Shopping gsh = sh.gfind(uid, 1, id);
		int s = 0;
		if(gsh==null) {
			//添加
			Shopping gsp = new Shopping(uid,sp.getEP_ID(),sp.getEP_PRICE(),1,1);
			s = sh.join(gsp);
		}else {
			//修改
			s = sh.update(gsh.getEOD_QUANTITY()+1,gsh.getEOD_COST()+sp.getEP_PRICE(), gsh.getEO_STATE(), gsh.getEOD_ID());
		}
		
		if (s > 0) {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('加入购物车成功！！');location.href='/Ebuy/frontend/product-view.jsp?id='+"+id+"</script>");
		} else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('加入购物车失败！！');location.href='/Ebuy/frontend/product-view.jsp?id='+"+id+"</script>");
		}
	}
	
	//修改购物车
	protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("update");
		
	}
	
	//删除购物车
	protected void del(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("del");
		int gid = Integer.parseInt(request.getParameter("gid"));
		System.out.println(gid);
		sh.del(gid);
		PrintWriter out = response.getWriter();
		response.sendRedirect("/Ebuy/Shopping/gfind");
	}

}
