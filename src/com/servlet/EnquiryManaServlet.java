package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.impl.EnquiryManaDaoimpl;
import com.entity.Comment;
import com.entity.PageBean;
// 后台留言
/**
 * Servlet implementation class EnquiryManaServlet
 */
@WebServlet("/EnquiryManaServlet/*")
public class EnquiryManaServlet extends BaseServlet {
	EnquiryManaDaoimpl enqim=new EnquiryManaDaoimpl();
	// 分页查询留言
	protected void seenqfind(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		String p = request.getParameter("p");
		int intp = 1;
		if (p != null) {
			intp = Integer.parseInt(p);
		}
		int encou=enqim.enqcou();
		PageBean pa=new PageBean();
		pa.setSize(6);
		pa.setTotal(encou);
		pa.setCurpage(intp);
		List enlis=enqim.enqfin(intp);
		request.setAttribute("enl", enlis);
		request.setAttribute("pagebean", pa);
		request.getRequestDispatcher("/manage/guestbook.jsp").forward(request, response);
	}

	// 删除留言
	protected void deenq(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String id=request.getParameter("eid");
		int eid=Integer.parseInt(id);
		int edsu=enqim.deenq(eid);
		if(edsu>0) {
			response.sendRedirect("/Ebuy/EnquiryManaServlet/seenqfind");
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('删除失败！');location.href='/Ebuy/EnquiryManaServlet/seenqfind'</script>");
		}
	}

	// 通过ID查找留言
	protected void fseid(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		String id=request.getParameter("eid");
		int enid=Integer.parseInt(id);
		request.getSession().setAttribute("id", enid);
		List lis=enqim.fenq(enid);
		request.setAttribute("enli", lis);
		request.getRequestDispatcher("/manage/guestbook-modify.jsp").forward(request, response);
	}
	
	//回复留言
	protected void repenq(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String hfid=request.getParameter("orderId");
		int reid=Integer.parseInt(hfid);
		String title=request.getParameter("ycnr");
		String crtime=request.getParameter("yctime");
		String hfnr=request.getParameter("replyContent");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String hftime=sdf.format(new Date());
		String hfna=request.getParameter("name");
		Comment com=new Comment(reid,title,crtime,hfnr,hftime,hfna);
		int resu=enqim.upenq(com);
		if(resu>0) {
			request.setAttribute("url", "/Ebuy/EnquiryManaServlet/seenqfind");
			request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
			/* response.sendRedirect("/Ebuy/EnquiryManaServlet/seenqfind"); */
		}else {
			String id=(String)request.getSession().getAttribute("id");
			PrintWriter out = response.getWriter();
			out.print("<script>alert('回复失败！');location.href='/Ebuy/EnquiryManaServlet/fseid?eid="+id+"'</script>");
		}
	}
	
}
