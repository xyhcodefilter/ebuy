package com.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.impl.ProductDaoimpl;
import com.dbutil.Upload;
import com.entity.PageBean;
import com.entity.Product;

@WebServlet("/Product/*")
public class ProductManaServlet extends BaseServlet {

	ProductDaoimpl pr = new ProductDaoimpl();

	//  添加
	protected void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// System.out.println("add");
		String url = Upload.geturl(request, "Ebuy/WebContent/images/product");
		// System.out.println(url);
		Product prod = Upload.imagesupload(request, url);
		// System.out.println(pr.getEP_NAME()+":"+pr.getEP_FILE_NAME());
		String name = prod.getEP_NAME();
		String ms = prod.getEP_DESCRIPTION();
		float price = prod.getEP_PRICE();
		int kc = prod.getEP_STOCK();
		int pid = prod.getEPC_CHILD_ID();
		String photo = prod.getEP_FILE_NAME();
//				//System.out.println(name+":"+ms+":"+price+":"+kc+":"+pid+":"+photo);
		int s = pr.add(name, ms, price, kc, pid, photo);
		request.setAttribute("url", "/Ebuy/Product/skip?tz=product-add.jsp");
		request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
	}

	// 修改
	protected void update(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, InterruptedException {
		// System.out.println("update");
		String url = Upload.geturl(request, "Ebuy/WebContent/images/product");
		Product prod = Upload.imagesupload(request, url);
		int id = prod.getEP_ID();
		String name = prod.getEP_NAME();
		String ms = prod.getEP_DESCRIPTION();
		float price = prod.getEP_PRICE();
		int kc = prod.getEP_STOCK();
		int pid = prod.getEPC_CHILD_ID();
		String photo = prod.getEP_FILE_NAME();
		// System.out.println(photo+":dd");
		int s = pr.update(name, ms, price, kc, pid, photo, id);
		request.setAttribute("url", "/Ebuy/Product/query");
		Thread.sleep(500);
		request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
		
	}

	// 删除
	protected void del(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// System.out.println("del");
		int id = Integer.parseInt(request.getParameter("id"));
		int s = pr.del(id);
		response.sendRedirect("/Ebuy/Product/query");
	}

	// 查询
	protected void query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// System.out.println("query");
		String p = request.getParameter("p");

		int intp = 1;
		if (p != null) {
			intp = Integer.parseInt(p);
		}
		int coun = pr.count();
		PageBean pagebean = new PageBean();
		pagebean.setSize(3);
		pagebean.setTotal(coun);
		pagebean.setCurpage(intp);
		List list = pr.pagequery(intp, 3);
		request.setAttribute("pagdata", list);// 请求里面放数据
		request.setAttribute("pagebean", pagebean);// 把总页数request
		request.getRequestDispatcher("/manage/product.jsp").forward(request, response);
	}

	// 跳转
	protected void skip(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// System.out.println("skip");
		String tz = request.getParameter("tz");
		request.getRequestDispatcher("/manage/" + tz).forward(request, response);
	}
}
