package com.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.impl.NewsDaoimpl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
// 新闻
/**
 * Servlet implementation class FirstPageServlet
 */
@WebServlet("/FirstPageServlet/*")
public class FirstPageServlet extends BaseServlet{
	NewsDaoimpl nim=new NewsDaoimpl();
	//新闻
	protected void onnews(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String id=request.getParameter("id");
		int nid=Integer.parseInt(id);
		List sed=nim.selid(nid);
		List seal=nim.news();
		List oldne=nim.oldnew();
		request.setAttribute("oldne", oldne);
		request.setAttribute("seid", sed);
		request.setAttribute("seall", seal);
		request.getRequestDispatcher("/frontend/news-view.jsp").forward(request, response);
	}
}
