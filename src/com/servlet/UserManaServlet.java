package com.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.dao.impl.UserManaDaoimpl;
import com.entity.PageBean;
import com.entity.User;

/**
 * Servlet implementation class UserManServlet
 */
@WebServlet("/UserManServlet/*")
public class UserManaServlet extends BaseServlet {
	UserManaDaoimpl sim = new UserManaDaoimpl();

	//  后台用户管理查询
	protected void muse(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		String p = request.getParameter("p");
		int intp = 1;
		if (p != null) {
			intp = Integer.parseInt(p);
		}
		int ucou = sim.cous();
		PageBean mp=new PageBean();
		mp.setSize(6);
		mp.setTotal(ucou);
		mp.setCurpage(intp);
		List mus = sim.muser(intp);
		request.setAttribute("mus", mus);
		request.setAttribute("pagebean", mp);
		request.getRequestDispatcher("/manage/user.jsp").forward(request, response);
	}

	// 删除用户
	protected void delus(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String eid = request.getParameter("eid");
		int id = Integer.parseInt(eid);
		int uidse = sim.usde(id);
		if (uidse > 0) {
			response.sendRedirect("/Ebuy/UserManServlet/muse");
		} else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('删除失败！');location.href='/Ebuy/UserManServlet/muse'</script>");
		}
	}

	// 添加用户
	protected void adse(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String uac = request.getParameter("userName");
		String name = request.getParameter("name");
		String pwd = request.getParameter("passWord");
		String sex = request.getParameter("sex");
		String timer = request.getParameter("timer");
		String phone = request.getParameter("mobile");
		String add = request.getParameter("address");
		String cardid=request.getParameter("cardid");
		String emal=request.getParameter("emal");
		User us=new User(uac,name,pwd,sex,timer,cardid,emal,phone,add,1);
		int as=sim.aduser(us);
		if(as>0) {
			request.setAttribute("url", "/Ebuy/UserManServlet/muse");
			request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
			/* response.sendRedirect("/Ebuy/UserManServlet/muse"); */
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('添加失败！');location.href='/Ebuy/manage/user-add.jsp'</script>");
		}
		
	}
	
	//通过id查用户
	protected void seid(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		String id=request.getParameter("uid");
		int uid=Integer.parseInt(id);
		List list=sim.seruser(uid);
		request.setAttribute("uscof", list);
		request.getRequestDispatcher("/manage/user-modify.jsp").forward(request, response);
	}
	
	//修改用户
	protected void upda(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String uid=request.getParameter("ycpq");
		int id=Integer.parseInt(uid);
		String acut=request.getParameter("userName");
		String name=request.getParameter("name");
		String pass=request.getParameter("passWord");
		String sex=request.getParameter("sex");
		String br=request.getParameter("birthyear");
		String ph=request.getParameter("mobile");
		String addr=request.getParameter("address");
		System.out.println(id+" "+acut+" "+name+" "+pass+" "+sex+" "+br+" "+ph+" "+addr);
		User us=new User(id,acut,name,pass,sex,br,ph,addr);
		int up=sim.upuser(us);
		if(up>0) {
			request.setAttribute("url", "/Ebuy/UserManServlet/muse");
			request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
			/* response.sendRedirect("/Ebuy/UserManServlet/muse"); */
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('修改失败！');location.href='/Ebuy/manage/user-modify.jsp'</script>");
		}
	}
}
