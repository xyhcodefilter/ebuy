package com.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.impl.OrderManaDaoimpl;
import com.dao.impl.ShoppingDaoImpl;
import com.dao.impl.UserDaoimpl;
import com.entity.Order;
import com.entity.User;

/**
 * Servlet implementation class PaySuccessful
 */
@WebServlet("/PaySuccessful/*")
public class PaySuccessful extends BaseServlet {
	UserDaoimpl us=new UserDaoimpl();
	OrderManaDaoimpl or=new OrderManaDaoimpl();
	ShoppingDaoImpl shimp=new ShoppingDaoImpl();
	// 支付成功
	protected void paysu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		HttpSession ssion = request.getSession();
		int uid = (Integer) ssion.getAttribute("userId");
		String je=request.getParameter("total_amount");
		float zje=Float.parseFloat(je);
		User user=us.seid(uid);
		String usid=String.valueOf(uid);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String time=sdf.format(new Date());
		Order ord=new Order(usid,user.getEU_USER_NAME(),user.getEU_MOBILE(),user.getEU_ADDRESS(),zje,time,1,2);
		int osu=or.addorder(ord);
		if(osu>0) {
			shimp.delsu(uid);
			response.sendRedirect("/Ebuy/frontend/index.jsp");
		}else {
			System.out.println("支付出错！");
		}
	}

}
