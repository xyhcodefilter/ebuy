package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.impl.NewsManaDaoimpl;
import com.entity.News;
import com.entity.PageBean;
// 新闻后台
/**
 * Servlet implementation class NewsManServlet
 */
@WebServlet("/NewsManServlet/*")
public class NewsManaServlet extends BaseServlet {
	NewsManaDaoimpl newim = new NewsManaDaoimpl();

	// 分页查询新闻
	protected void newfind(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		String p = request.getParameter("p");
		int intp = 1;
		if (p != null) {
			intp = Integer.parseInt(p);
		}
		int newc = newim.necoun();
		PageBean nep = new PageBean();
		nep.setSize(6);
		nep.setTotal(newc);
		nep.setCurpage(intp);
		List nelis = newim.newfind(intp);
		request.setAttribute("newlis", nelis);
		request.setAttribute("pagebean", nep);
		request.getRequestDispatcher("/manage/news.jsp").forward(request, response);
	}

	// 删除新闻
	protected void dene(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		String id = request.getParameter("neid");
		int nid = Integer.parseInt(id);
		int dsuc = newim.denew(nid);
		if (dsuc > 0) {
			request.setAttribute("url", "/Ebuy/NewsManServlet/newfind");
			request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
			/* response.sendRedirect("/Ebuy/NewsManServlet/newfind"); */
		} else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('删除失败！');location.href='/Ebuy/NewsManServlet/newfind'</script>");
		}
	}

	// 通过id查找新闻
	protected void senewcof(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		
		String id = request.getParameter("upeid");
		int neid = Integer.parseInt(id);
		List list = newim.neidse(neid);
		request.setAttribute("newsco", list);
		request.getRequestDispatcher("/manage/news-modify.jsp").forward(request, response);
	}

	// 修改新闻
	protected void upnew(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String id=request.getParameter("ycid");
		int nid=Integer.parseInt(id);
		String title=request.getParameter("title");
		String cont=request.getParameter("content");
		String time=request.getParameter("ycsj");
		News ne=new News(nid,title,cont,time);
		int neup=newim.upnew(ne);
		if(neup>0) {
			request.setAttribute("url", "/Ebuy/NewsManServlet/newfind");
			request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
			/* response.sendRedirect("/Ebuy/NewsManServlet/newfind"); */
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('修改失败！');location.href='/Ebuy/manage/news-modify.jsp'</script>");
		}
	}

	// 添加新闻
	protected void addnew(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String title=request.getParameter("title");
		String cont=request.getParameter("content");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String time=sdf.format(new Date());
		News news=new News(title,cont,time);
		int adsu=newim.addnew(news);
		if(adsu>0) {
			/* response.sendRedirect("/Ebuy/NewsManServlet/newfind"); */
			request.setAttribute("url", "/Ebuy/NewsManServlet/newfind");
			request.getRequestDispatcher("/manage/manage-result.jsp").forward(request, response);
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('添加失败！');location.href='/Ebuy/manage/news-add.jsp'</script>");
		}
	}

}
