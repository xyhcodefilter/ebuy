package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.impl.NewsDaoimpl;
import com.dao.impl.UserDaoimpl;
import com.entity.User;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
// 登入
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet/*")

public class LoginServlet extends BaseServlet {
	UserDaoimpl sim=new UserDaoimpl();
	
	//图形验证
	protected void yzm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(170, 100);
		String code=lineCaptcha.getCode();
		request.getSession().setAttribute("cod", code);
		lineCaptcha.write(response.getOutputStream());
	}
	//登入
	protected void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		// TODO Auto-generated method stub
		
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String name=request.getParameter("userName");
		String pass=request.getParameter("passWord");
		String oldcode=(String) request.getSession().getAttribute("cod");
		String srcode=request.getParameter("veryCode");
		User use=sim.login(name, pass);
		int userId = use.getEU_USER_ID();
		int zt=use.getEU_STATUS();
		String uname=use.getEU_USER_NAME();
		if(use!=null && srcode.equals(oldcode) && zt==1) {
			System.out.println("姓名："+uname);
			request.getSession().setAttribute("name", uname);
			request.getSession().setAttribute("userId", userId);
			response.sendRedirect("/Ebuy/frontend/index.jsp");
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('登入失败！');location.href='/Ebuy/frontend/login.jsp'</script>");
		}
		
	}
	
	//注册
	protected void reg(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String name=request.getParameter("userName");
		String acou=request.getParameter("userAc");
		String sex=request.getParameter("userSex");
		String birth=request.getParameter("userBirthday");
		String idcard=request.getParameter("userIdentity");
		String email=request.getParameter("userEmail");
		String phone=request.getParameter("userMobile");
		String address=request.getParameter("userAddress");
		String type=request.getParameter("userStatus");
		int typenum=Integer.parseInt(type);
		String pwd=request.getParameter("rePassWord");
		String code=request.getParameter("veryCode");
		String oldcode=(String) request.getSession().getAttribute("cod");
		
		if(name!=null && sex!=null && birth!=null && idcard!=null && email!=null && phone!=null && address!=null && type!=null && pwd!=null && code.equals(oldcode) && acou!=null) {
			User us=new User(acou,name,pwd,sex,birth,idcard,email,phone,address,typenum);
			int re=sim.register(us);
			if(re>0) {
				if(typenum==1) {
					request.setAttribute("url", "/Ebuy/frontend/login.jsp");
				}else {
					request.setAttribute("url", "/Ebuy/frontend/BackstageMana.jsp");
				}
				request.getRequestDispatcher("/frontend/reg-result.jsp").forward(request, response);
			}else {
				PrintWriter out = response.getWriter();
				out.print("<script>alert('注册失败！');location.href='/Ebuy/frontend/register.jsp'</script>");
			}
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('注册失败！');location.href='/Ebuy/frontend/register.jsp'</script>");
		}
		
	}
	
	//退出
	protected void exit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		request.getSession().invalidate();
		response.sendRedirect("/Ebuy/frontend/index.jsp");
	}
	
	//登入后台
	protected void logMan(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String name=request.getParameter("name");
		String pas=request.getParameter("pass");
		String co=request.getParameter("code");
		String oldcode=(String) request.getSession().getAttribute("cod2");
		User use=sim.login(name, pas);
		int zt=use.getEU_STATUS();
		String uname=use.getEU_USER_NAME();
		if(use!=null && co.equals(oldcode) && zt==2) {
			request.getSession().setAttribute("adname", uname);
			response.sendRedirect("/Ebuy/manage/index.jsp");
		}else {
			PrintWriter out = response.getWriter();
			out.print("<script>alert('登入失败！');location.href='/Ebuy/frontend/BackstageMana.jsp'</script>");
		}
	}
	
	//后台验证码
	protected void yzm2(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(150, 50);
		String code=lineCaptcha.getCode();
		request.getSession().setAttribute("cod2", code);
		lineCaptcha.write(response.getOutputStream());
	}
}
