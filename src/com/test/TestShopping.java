package com.test;

import org.junit.Test;

import com.dao.impl.ShoppingDaoImpl;
import com.entity.Shopping;

public class TestShopping {
	
	ShoppingDaoImpl sh = new ShoppingDaoImpl();
	
	//加入购物车
	@Test
	public void t1(){
		Shopping sing = new Shopping(1,56,8,60,1);
		System.out.println(sh.join(sing));
	}
	
	//修改购物车
	@Test
	public void t2(){
		System.out.println(sh.update(4, 60, 1, 4));
	}
	
	//删除购物车
	@Test
	public void t3(){
		System.out.println(sh.del(5));
	}
	
	
	//查询购物车(用户id，1:未下单，2:已下单)
	@Test
	public void t4(){
		System.out.println(sh.gfind(1, 1));
	}
	
	//合计(用户id)
	@Test
	public void t5(){
		System.out.println(sh.getTotal(1));
	}
	
}
