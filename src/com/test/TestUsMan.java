package com.test;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.dao.impl.UserManaDaoimpl;
import com.entity.User;

public class TestUsMan {
	UserManaDaoimpl usim=new UserManaDaoimpl();
	
	//测试分页
	@Test
	public void testusfind() throws SQLException {
		List list=usim.muser(1);
		System.out.println(list);
	}
	
	//测试用户总数
	@Test
	public void testco() throws SQLException {
		int us=usim.cous();
		System.out.println(us);
	}
	
	//测试添加用户
	@Test
	public void testadduse() throws SQLException {
		User us=new User("lisi","李思","12345","女","2001-10-1","387221200110015431","lisi@outlook.com","18709213321","广州市白云区",1);
		int ads=usim.aduser(us);
		System.out.println(ads);
	}
	
	//测试ID查询用户
	@Test
	public void testseidus() throws SQLException {
		List list=usim.seruser(71);
		System.out.println(list);
	}
	
	//测试修改
	@Test
	public void testup() throws SQLException {
		User us=new User(71,"fangdodo","方泽泽","12345","男","2011-01-05","12128112131","广东江门");
		int tup=usim.upuser(us);
		System.out.println(tup);
	}
}
