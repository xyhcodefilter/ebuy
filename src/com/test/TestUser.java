package com.test;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.dao.impl.UserDaoimpl;
import com.entity.User;

public class TestUser {
	UserDaoimpl usim=new UserDaoimpl();
	
	//测试登入
	@Test
	public void Testlo() throws SQLException {
		User user=usim.login("admin", "admin");
		System.out.println(user);
	}
	
	//测试注册
	@Test
	public void Testresu() throws SQLException {
		User us=new User("lisi","李四","12345","男","2003-10-14","360231300310141112","lisi@outlook.com","18309872451","广东省广州市花都区",2);
		int tre=usim.register(us);
		System.out.println(tre);
	}
	
	//查id
	@Test
	public void Testsid() throws SQLException {
		User us=usim.seid(66);
		System.out.println(us);
	}
}
