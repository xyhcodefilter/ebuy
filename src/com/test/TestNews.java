package com.test;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.dao.impl.NewsDaoimpl;

public class TestNews {
	
	NewsDaoimpl nim=new NewsDaoimpl();
	
	//测试新闻
	@Test
	public void testnew() throws SQLException {
		List lis=nim.news();
		System.out.println(lis);
	}
	
	//id查询
	
	@Test
	public void testseid() throws SQLException {
		List selid=nim.selid(3);
		System.out.println(selid);
	}
	
	//查询老新闻
	@Test
	public void testonew() throws SQLException {
		List se=nim.oldnew();
		System.out.println(se);
	}
}
