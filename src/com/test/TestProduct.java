package com.test;

import java.util.List;

import org.junit.Test;

import com.dao.impl.ProductDaoimpl;
import com.entity.Product;


public class TestProduct {
	
	ProductDaoimpl p = new ProductDaoimpl();
	
	//测试添加
	@Test
	public void Textadd() {
		int s = p.add("iphone11","手机",8000,564,651,"4.jpg");
		System.out.println(s);
	}
	
	//测试查询
	@Test
	public void Textquery() {
		List list = p.query();
		System.out.println(list);
		
		Product d = p.query(591);
		System.out.println(d);
		
		List list2 = p.querylist(546);
		System.out.println(list2);
	}
	
	//测试修改
	@Test
	public void Textupdate() {
		int s = p.update("ssd","acddd",3,2,1,"6.jpg",666);
		System.out.println(s);
	}
	
	//测试删除
	@Test
	public void Textdel() {
		int s = p.del(666);
		System.out.println(s);
	}
	
	//今日特价2次
	@Test
	public void testjr() {
		List lis=p.Sale();
		System.out.println(lis);
	}
	
	
}
