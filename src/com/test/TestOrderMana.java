package com.test;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.dao.impl.OrderManaDaoimpl;
import com.entity.Order;

public class TestOrderMana {
	// 后台订单测试
	OrderManaDaoimpl oimp = new OrderManaDaoimpl();

	// 分页查询所有订单
	@Test
	public void testseinf() throws SQLException {
		List list = oimp.orall(1);
		System.out.println(list);
	}

	// 统计所有订单的条数
	@Test
	public void testseco() throws SQLException {
		int olen = oimp.couorall();
		System.out.println(olen);
	}

	// 通过ID查订单
	@Test
	public void testor() throws SQLException {
		List list = oimp.orconfig(6);
		System.out.println(list);
	}

	// 修改订单
	@Test
	public void testupor() throws SQLException {
		Order or = new Order(7, "方泽泽7", "广东江门7", 21, "2021-06-03", 3);
		int up = oimp.upor(or);
		System.out.println(up);
	}

	// 通过ID查找
	@Test
	public void testsid() throws SQLException {
		List list = oimp.sorid(1,6022);
		System.out.println(list);
	}

	// 通过name查找
	@Test
	public void testsna() throws SQLException {
		List list = oimp.sornam(1,"方");
		System.out.println(list);
	}

	// 通过id name查找
	@Test
	public void testsnaid() throws SQLException {
		List list = oimp.soridna(1,60221, "方");
		System.out.println(list);
	}

	// 通过ID统计人数
	@Test
	public void testsd() throws SQLException {
		int idnum = oimp.secoid(602);
		System.out.println(idnum);
	}

	// 通过name统计人数
	@Test
	public void testsn() throws SQLException {
		int nanum = oimp.secona("方");
		System.out.println(nanum);
	}

	// 通过IDname统计人数
	@Test
	public void testsdn() throws SQLException {
		int idnanum = oimp.secoina(602, "李");
		System.out.println(idnanum);
	}
}
