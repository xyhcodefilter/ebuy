package com.test;

import java.util.List;

import org.junit.Test;

import com.dao.impl.ProductCategoryDaoimpl;
import com.entity.ProductCategory;


public class TestProductCategory {
	
	ProductCategoryDaoimpl p = new ProductCategoryDaoimpl();
	
	//测试添加
	@Test
	public void Textadd() {
		int s = p.add("大理石",0);
		System.out.println(s);
	}
	
	//测试查询
	@Test
	public void Textquery() {
		List list = p.query();
		System.out.println(list);
		
		ProductCategory ds = p.query(631);
		System.out.println(ds);
		
		List list2 = p.pagequery(2,3);
		System.out.println(list2);
		
		List list3 = p.querylist(580);
		System.out.println(list3);
	}
	
	//测试统计
	@Test
	public void Textcount() {
		int s = p.count();
		System.out.println(s);
	}
	
	//测试修改
	@Test
	public void Textupdate() {
		int s = p.update(632,"ssd",0);
		System.out.println(s);
	}
	
	//测试删除
	@Test
	public void Textdel() {
		int s = p.del(632);
		System.out.println(s);
	}
	
	//测试查询id类
	@Test
	public void testseid() {
		List list=p.secate(16);
		System.out.println(list);
	}
}
