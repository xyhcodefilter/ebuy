package com.test;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.dao.impl.EnquiryManaDaoimpl;
import com.entity.Comment;
//留言管理
public class TestCommMana {
	EnquiryManaDaoimpl imp=new EnquiryManaDaoimpl();
	
	//测后台分页留言
	@Test
	public void testefin() throws SQLException {
		List list=imp.enqfin(2);
		System.out.println(list);
	}
	
	//测回复留言
	@Test
	public void testhfenq() throws SQLException {
		Comment com=new Comment(686,"荣耀笔记本会发烫嘛？","2021-05-30","不会2 但是噪音很大！","2020-06-02","王强");
		int hf=imp.upenq(com);
		System.out.println(hf);
	}
	
	//删除留言
	@Test
	public void testdeenq() throws SQLException {
		int desu=imp.deenq(722);
		System.out.println(desu);
	}
	
	//统计留言的数量
	@Test
	public void testcou() throws SQLException {
		int coun=imp.enqcou();
		System.out.println(coun);
	}
	
	//通过ID查找留言
	@Test
	public void testfid() throws SQLException {
		List list=imp.fenq(683);
		System.out.println(list);
	}
}
