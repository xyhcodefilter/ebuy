package com.test;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.dao.impl.NewsManaDaoimpl;
import com.entity.News;


public class TestNewMan {
	//测新闻管理
	NewsManaDaoimpl imp=new NewsManaDaoimpl();
	
	//测分页查询
	@Test
	public void testusfind() throws SQLException {
		List list=imp.newfind(1);
		System.out.println(list);
	}
	
	//测试新闻总条数
	@Test
	public void testnecou() throws SQLException {
		int newlen=imp.necoun();
		System.out.println(newlen);
	}
	
	
	//测试删除新闻
	@Test
	public void testne() throws SQLException {
		int de=imp.denew(657);
		System.out.println(de);
	}
	
	//通过ID查找
	@Test
	public void testseid() throws SQLException {
		List list =imp.neidse(659);
		System.out.println(list);
	}
	
	//修改新闻
	@Test
	public void testupnew() throws SQLException {
		News news=new News(659,"最新品电脑","最新电脑华硕@豆","2019-01-10");
		int ups=imp.upnew(news);
		System.out.println(ups);
	}
	
	//添加新闻
	@Test
	public void testaddnew() throws SQLException {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String time=sdf.format(new Date());
		News anew=new News("iphone 13新上架","iphone新上架 首周打9折",time);
		int ad=imp.addnew(anew);
		System.out.println(ad);
	}
}
