package com.entity;

public class PageBean {
	//页数
	int total;//总行；
	int page;//总页数
	int curpage;//当前页
	int size;//每页几条
	int begin,end;//开始结束页码
	
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
		//求总页数
		page=total/size;
		if(total%size!=0) {
			page++;
		}
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getCurpage() {
		return curpage;
	}
	public void setCurpage(int curpage) {
		this.curpage = curpage;
		//当前=8 +4 -5
		if(page<=10) {
			begin=1;
			end=page;
		}else {
			begin=curpage-5;
			end=curpage+4;
			//头超出
			if(begin<=0) {
				begin=1;
				end=10;
			}
			//尾部超出
			if(end>page) {
				end=page;
				begin=page-9;
			}
		}
		
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getBegin() {
		return begin;
	}
	public void setBegin(int begin) {
		this.begin = begin;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	
}
