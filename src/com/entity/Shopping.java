package com.entity;

public class Shopping {
	  //商品
	  int EOD_ID;
	  int EO_USER_ID;
	  int EP_ID;
	  int EOD_QUANTITY;
	  float EOD_COST;
	  int EO_STATE;
	public int getEOD_ID() {
		return EOD_ID;
	}
	public void setEOD_ID(int eOD_ID) {
		EOD_ID = eOD_ID;
	}
	public int getEO_USER_ID() {
		return EO_USER_ID;
	}
	public void setEO_USER_ID(int eO_USER_ID) {
		EO_USER_ID = eO_USER_ID;
	}
	public int getEP_ID() {
		return EP_ID;
	}
	public void setEP_ID(int eP_ID) {
		EP_ID = eP_ID;
	}
	public int getEOD_QUANTITY() {
		return EOD_QUANTITY;
	}
	public void setEOD_QUANTITY(int eOD_QUANTITY) {
		EOD_QUANTITY = eOD_QUANTITY;
	}
	public float getEOD_COST() {
		return EOD_COST;
	}
	public void setEOD_COST(float eOD_COST) {
		EOD_COST = eOD_COST;
	}
	public int getEO_STATE() {
		return EO_STATE;
	}
	public void setEO_STATE(int eO_STATE) {
		EO_STATE = eO_STATE;
	}
	  
	public Shopping() {
		
	}
	public Shopping(int eOD_ID, int eO_USER_ID, int eP_ID, int eOD_QUANTITY, float eOD_COST, int eO_STATE) {
		super();
		EOD_ID = eOD_ID;
		EO_USER_ID = eO_USER_ID;
		EP_ID = eP_ID;
		EOD_QUANTITY = eOD_QUANTITY;
		EOD_COST = eOD_COST;
		EO_STATE = eO_STATE;
	}
	
	public Shopping(int eO_USER_ID, int eP_ID, float eOD_COST,int eOD_QUANTITY, int eO_STATE) {
		super();
		EO_USER_ID = eO_USER_ID;
		EP_ID = eP_ID;
		EOD_QUANTITY = eOD_QUANTITY;
		EOD_COST = eOD_COST;
		EO_STATE = eO_STATE;
	}
	
	
	
	@Override
	public String toString() {
		return "ShoppingDao [EOD_ID=" + EOD_ID + ", EO_USER_ID=" + EO_USER_ID + ", EP_ID=" + EP_ID + ", EOD_QUANTITY="
				+ EOD_QUANTITY + ", EOD_COST=" + EOD_COST + ", EO_STATE=" + EO_STATE + "]";
	}
	
	
}
