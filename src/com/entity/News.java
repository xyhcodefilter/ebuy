package com.entity;

import java.util.Date;

public class News {
	//新闻
	int EN_ID;//id
	String EN_TITLE;//标题
	String EN_CONTENT;//内容
	String EN_CREATE_TIME;//时间
	public int getEN_ID() {
		return EN_ID;
	}
	public void setEN_ID(int eN_ID) {
		EN_ID = eN_ID;
	}
	public String getEN_TITLE() {
		return EN_TITLE;
	}
	public void setEN_TITLE(String eN_TITLE) {
		EN_TITLE = eN_TITLE;
	}
	public String getEN_CONTENT() {
		return EN_CONTENT;
	}
	public void setEN_CONTENT(String eN_CONTENT) {
		EN_CONTENT = eN_CONTENT;
	}
	public String getEN_CREATE_TIME() {
		return EN_CREATE_TIME;
	}
	public void setEN_CREATE_TIME(String eN_CREATE_TIME) {
		EN_CREATE_TIME = eN_CREATE_TIME;
	}
	
	
	public News() {
		
	}
	
	public News(String eN_TITLE, String eN_CONTENT, String eN_CREATE_TIME) {
		super();
		EN_TITLE = eN_TITLE;
		EN_CONTENT = eN_CONTENT;
		EN_CREATE_TIME = eN_CREATE_TIME;
	}
	
	public News(int eN_ID, String eN_TITLE, String eN_CONTENT, String eN_CREATE_TIME) {
		super();
		EN_ID = eN_ID;
		EN_TITLE = eN_TITLE;
		EN_CONTENT = eN_CONTENT;
		EN_CREATE_TIME = eN_CREATE_TIME;
	}
	
	
	@Override
	public String toString() {
		return "News [EN_ID=" + EN_ID + ", EN_TITLE=" + EN_TITLE + ", EN_CONTENT=" + EN_CONTENT + ", EN_CREATE_TIME="
				+ EN_CREATE_TIME + "]";
	}
	
	
}
