package com.entity;

public class User {
	//用户
	int EU_USER_ID;//id
	String EU_USER_Account;//账号
	String EU_USER_NAME; //姓名
	String EU_PASSWORD; //密码
	String EU_SEX; //性别
	String EU_BIRTHDAY;//生日 
	String EU_IDENTITY_CODE;//身份证֤
	String EU_EMAIL;//电子邮件
	String EU_MOBILE;//电话
	String EU_ADDRESS;//地址  
	int EU_STATUS;//状态
	public int getEU_USER_ID() {
		return EU_USER_ID;
	}
	public void setEU_USER_ID(int eU_USER_ID) {
		EU_USER_ID = eU_USER_ID;
	}
	public String getEU_USER_Account() {
		return EU_USER_Account;
	}
	public void setEU_USER_Account(String eU_USER_Account) {
		EU_USER_Account = eU_USER_Account;
	}
	public String getEU_USER_NAME() {
		return EU_USER_NAME;
	}
	public void setEU_USER_NAME(String eU_USER_NAME) {
		EU_USER_NAME = eU_USER_NAME;
	}
	public String getEU_PASSWORD() {
		return EU_PASSWORD;
	}
	public void setEU_PASSWORD(String eU_PASSWORD) {
		EU_PASSWORD = eU_PASSWORD;
	}
	public String getEU_SEX() {
		return EU_SEX;
	}
	public void setEU_SEX(String eU_SEX) {
		EU_SEX = eU_SEX;
	}
	public String getEU_BIRTHDAY() {
		return EU_BIRTHDAY;
	}
	public void setEU_BIRTHDAY(String eU_BIRTHDAY) {
		EU_BIRTHDAY = eU_BIRTHDAY;
	}
	public String getEU_IDENTITY_CODE() {
		return EU_IDENTITY_CODE;
	}
	public void setEU_IDENTITY_CODE(String eU_IDENTITY_CODE) {
		EU_IDENTITY_CODE = eU_IDENTITY_CODE;
	}
	public String getEU_EMAIL() {
		return EU_EMAIL;
	}
	public void setEU_EMAIL(String eU_EMAIL) {
		EU_EMAIL = eU_EMAIL;
	}
	public String getEU_MOBILE() {
		return EU_MOBILE;
	}
	public void setEU_MOBILE(String eU_MOBILE) {
		EU_MOBILE = eU_MOBILE;
	}
	public String getEU_ADDRESS() {
		return EU_ADDRESS;
	}
	public void setEU_ADDRESS(String eU_ADDRESS) {
		EU_ADDRESS = eU_ADDRESS;
	}
	public int getEU_STATUS() {
		return EU_STATUS;
	}
	public void setEU_STATUS(int eU_STATUS) {
		EU_STATUS = eU_STATUS;
	}
	
	
	public User() {
		
	}
	public User(String eU_USER_Account, String eU_USER_NAME, String eU_PASSWORD, String eU_SEX, String eU_BIRTHDAY,
			String eU_IDENTITY_CODE, String eU_EMAIL, String eU_MOBILE, String eU_ADDRESS, int eU_STATUS) {
		super();
		EU_USER_Account = eU_USER_Account;
		EU_USER_NAME = eU_USER_NAME;
		EU_PASSWORD = eU_PASSWORD;
		EU_SEX = eU_SEX;
		EU_BIRTHDAY = eU_BIRTHDAY;
		EU_IDENTITY_CODE = eU_IDENTITY_CODE;
		EU_EMAIL = eU_EMAIL;
		EU_MOBILE = eU_MOBILE;
		EU_ADDRESS = eU_ADDRESS;
		EU_STATUS = eU_STATUS;
	}
	
	public User(int eU_USER_ID, String eU_USER_Account, String eU_USER_NAME, String eU_PASSWORD, String eU_SEX,
			String eU_BIRTHDAY, String eU_IDENTITY_CODE, String eU_EMAIL, String eU_MOBILE, String eU_ADDRESS,
			int eU_STATUS) {
		super();
		EU_USER_ID = eU_USER_ID;
		EU_USER_Account = eU_USER_Account;
		EU_USER_NAME = eU_USER_NAME;
		EU_PASSWORD = eU_PASSWORD;
		EU_SEX = eU_SEX;
		EU_BIRTHDAY = eU_BIRTHDAY;
		EU_IDENTITY_CODE = eU_IDENTITY_CODE;
		EU_EMAIL = eU_EMAIL;
		EU_MOBILE = eU_MOBILE;
		EU_ADDRESS = eU_ADDRESS;
		EU_STATUS = eU_STATUS;
	}
	
	
	
	
	
	public User(int eU_USER_ID, String eU_USER_Account, String eU_USER_NAME, String eU_PASSWORD, String eU_SEX,
			String eU_BIRTHDAY, String eU_MOBILE, String eU_ADDRESS) {
		EU_USER_ID = eU_USER_ID;
		EU_USER_Account = eU_USER_Account;
		EU_USER_NAME = eU_USER_NAME;
		EU_PASSWORD = eU_PASSWORD;
		EU_SEX = eU_SEX;
		EU_BIRTHDAY = eU_BIRTHDAY;
		EU_MOBILE = eU_MOBILE;
		EU_ADDRESS = eU_ADDRESS;
	}
	
	
	@Override
	public String toString() {
		return "User [EU_USER_ID=" + EU_USER_ID + ", EU_USER_Account=" + EU_USER_Account + ", EU_USER_NAME="
				+ EU_USER_NAME + ", EU_PASSWORD=" + EU_PASSWORD + ", EU_SEX=" + EU_SEX + ", EU_BIRTHDAY=" + EU_BIRTHDAY
				+ ", EU_IDENTITY_CODE=" + EU_IDENTITY_CODE + ", EU_EMAIL=" + EU_EMAIL + ", EU_MOBILE=" + EU_MOBILE
				+ ", EU_ADDRESS=" + EU_ADDRESS + ", EU_STATUS=" + EU_STATUS + "]";
	}
	
	
}
