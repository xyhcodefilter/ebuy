package com.entity;

public class ProductCategory {
	int EPC_ID;      //id
	String EPC_NAME;   //类型
	int EPC_PARENT_ID;  //
	public int getEPC_ID() {
		return EPC_ID;
	}
	public void setEPC_ID(int ePC_ID) {
		EPC_ID = ePC_ID;
	}
	public String getEPC_NAME() {
		return EPC_NAME;
	}
	public void setEPC_NAME(String ePC_NAME) {
		EPC_NAME = ePC_NAME;
	}
	public int getEPC_PARENT_ID() {
		return EPC_PARENT_ID;
	}
	public void setEPC_PARENT_ID(int ePC_PARENT_ID) {
		EPC_PARENT_ID = ePC_PARENT_ID;
	}
	
	public ProductCategory() {
		
	}
	
	public ProductCategory(int ePC_ID, String ePC_NAME, int ePC_PARENT_ID) {
		super();
		EPC_ID = ePC_ID;
		EPC_NAME = ePC_NAME;
		EPC_PARENT_ID = ePC_PARENT_ID;
	}
	
	@Override 
	public String toString() {
		return "ProductCategory [EPC_ID=" + EPC_ID + ", EPC_NAME=" + EPC_NAME + ", EPC_PARENT_ID=" + EPC_PARENT_ID
				+ "]";
	}
	
	
}
