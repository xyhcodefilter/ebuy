package com.entity;

import java.util.Date;

public class Comment {
	//留言
	int EC_ID;//id
	String EC_CONTENT;//内容
	String EC_CREATE_TIME;//发布时间
	String EC_REPLY;//回复
	String EC_REPLY_TIME;//回复时间
	String EC_NICK_NAME;//留言昵称
	public int getEC_ID() {
		return EC_ID;
	}
	public void setEC_ID(int eC_ID) {
		EC_ID = eC_ID;
	}
	public String getEC_CONTENT() {
		return EC_CONTENT;
	}
	public void setEC_CONTENT(String eC_CONTENT) {
		EC_CONTENT = eC_CONTENT;
	}
	public String getEC_CREATE_TIME() {
		return EC_CREATE_TIME;
	}
	public void setEC_CREATE_TIME(String eC_CREATE_TIME) {
		EC_CREATE_TIME = eC_CREATE_TIME;
	}
	public String getEC_REPLY() {
		return EC_REPLY;
	}
	public void setEC_REPLY(String eC_REPLY) {
		EC_REPLY = eC_REPLY;
	}
	public String getEC_REPLY_TIME() {
		return EC_REPLY_TIME;
	}
	public void setEC_REPLY_TIME(String eC_REPLY_TIME) {
		EC_REPLY_TIME = eC_REPLY_TIME;
	}
	public String getEC_NICK_NAME() {
		return EC_NICK_NAME;
	}
	public void setEC_NICK_NAME(String eC_NICK_NAME) {
		EC_NICK_NAME = eC_NICK_NAME;
	}
	
	public Comment() {
		
	}
	
	public Comment(String eC_CONTENT, String eC_CREATE_TIME, String eC_REPLY, String eC_REPLY_TIME,
			String eC_NICK_NAME) {
		super();
		EC_CONTENT = eC_CONTENT;
		EC_CREATE_TIME = eC_CREATE_TIME;
		EC_REPLY = eC_REPLY;
		EC_REPLY_TIME = eC_REPLY_TIME;
		EC_NICK_NAME = eC_NICK_NAME;
	}
	
	public Comment(int eC_ID, String eC_CONTENT, String eC_CREATE_TIME, String eC_REPLY, String eC_REPLY_TIME,
			String eC_NICK_NAME) {
		super();
		EC_ID = eC_ID;
		EC_CONTENT = eC_CONTENT;
		EC_CREATE_TIME = eC_CREATE_TIME;
		EC_REPLY = eC_REPLY;
		EC_REPLY_TIME = eC_REPLY_TIME;
		EC_NICK_NAME = eC_NICK_NAME;
	}
	@Override
	public String toString() {
		return "Comment [EC_ID=" + EC_ID + ", EC_CONTENT=" + EC_CONTENT + ", EC_CREATE_TIME=" + EC_CREATE_TIME
				+ ", EC_REPLY=" + EC_REPLY + ", EC_REPLY_TIME=" + EC_REPLY_TIME + ", EC_NICK_NAME=" + EC_NICK_NAME
				+ "]";
	}
	
	
}
