package com.entity;

/**
 ** 商品类
 **/
public class Product {
	int EP_ID; // id
	String EP_NAME; // 名称
	String EP_DESCRIPTION; // 描述
	float EP_PRICE; // 单价
	int EP_STOCK; // 库存
	int EPC_CHILD_ID; // 分类ID
	String EP_FILE_NAME; // 图片名称

	public int getEP_ID() {
		return EP_ID;
	}

	public void setEP_ID(int eP_ID) {
		EP_ID = eP_ID;
	}

	public String getEP_NAME() {
		return EP_NAME;
	}

	public void setEP_NAME(String eP_NAME) {
		EP_NAME = eP_NAME;
	}

	public String getEP_DESCRIPTION() {
		return EP_DESCRIPTION;
	}

	public void setEP_DESCRIPTION(String eP_DESCRIPTION) {
		EP_DESCRIPTION = eP_DESCRIPTION;
	}

	public float getEP_PRICE() {
		return EP_PRICE;
	}

	public void setEP_PRICE(float eP_PRICE) {
		EP_PRICE = eP_PRICE;
	}

	public int getEP_STOCK() {
		return EP_STOCK;
	}

	public void setEP_STOCK(int eP_STOCK) {
		EP_STOCK = eP_STOCK;
	}

	public int getEPC_CHILD_ID() {
		return EPC_CHILD_ID;
	}

	public void setEPC_CHILD_ID(int ePC_CHILD_ID) {
		EPC_CHILD_ID = ePC_CHILD_ID;
	}

	public String getEP_FILE_NAME() {
		return EP_FILE_NAME;
	}

	public void setEP_FILE_NAME(String eP_FILE_NAME) {
		EP_FILE_NAME = eP_FILE_NAME;
	}

	public Product(int eP_ID, String eP_NAME, String eP_DESCRIPTION, float eP_PRICE, int eP_STOCK, int ePC_CHILD_ID,
			String eP_FILE_NAME) {
		super();
		EP_ID = eP_ID;
		EP_NAME = eP_NAME;
		EP_DESCRIPTION = eP_DESCRIPTION;
		EP_PRICE = eP_PRICE;
		EP_STOCK = eP_STOCK;
		EPC_CHILD_ID = ePC_CHILD_ID;
		EP_FILE_NAME = eP_FILE_NAME;
	}

	public Product() {
		super();
	}

	@Override
	public String toString() {
		return "Product [EP_ID=" + EP_ID + ", EP_NAME=" + EP_NAME + ", EP_DESCRIPTION=" + EP_DESCRIPTION + ", EP_PRICE="
				+ EP_PRICE + ", EP_STOCK=" + EP_STOCK + ", EPC_CHILD_ID=" + EPC_CHILD_ID + ", EP_FILE_NAME="
				+ EP_FILE_NAME + "]\n";
	}
}
