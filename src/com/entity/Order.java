package com.entity;
//订单
public class Order {
	int EO_ID;
	String EO_USER_ID;
	String EO_USER_NAME;
	String EO_USER_TEL;
	String EO_USER_ADDRESS;
	float EO_COST;
	String EO_CREATE_TIME;
	int EO_STATUS;
	int EO_TYPE;
	
	public int getEO_ID() {
		return EO_ID;
	}
	public void setEO_ID(int eO_ID) {
		EO_ID = eO_ID;
	}
	public String getEO_USER_ID() {
		return EO_USER_ID;
	}
	public void setEO_USER_ID(String eO_USER_ID) {
		EO_USER_ID = eO_USER_ID;
	}
	public String getEO_USER_NAME() {
		return EO_USER_NAME;
	}
	public void setEO_USER_NAME(String eO_USER_NAME) {
		EO_USER_NAME = eO_USER_NAME;
	}
	public String getEO_USER_TEL() {
		return EO_USER_TEL;
	}
	public void setEO_USER_TEL(String eO_USER_TEL) {
		EO_USER_TEL = eO_USER_TEL;
	}
	public String getEO_USER_ADDRESS() {
		return EO_USER_ADDRESS;
	}
	public void setEO_USER_ADDRESS(String eO_USER_ADDRESS) {
		EO_USER_ADDRESS = eO_USER_ADDRESS;
	}
	public float getEO_COST() {
		return EO_COST;
	}
	public void setEO_COST(float eO_COST) {
		EO_COST = eO_COST;
	}
	public String getEO_CREATE_TIME() {
		return EO_CREATE_TIME;
	}
	public void setEO_CREATE_TIME(String eO_CREATE_TIME) {
		EO_CREATE_TIME = eO_CREATE_TIME;
	}
	public int getEO_STATUS() {
		return EO_STATUS;
	}
	public void setEO_STATUS(int eO_STATUS) {
		EO_STATUS = eO_STATUS;
	}
	public int getEO_TYPE() {
		return EO_TYPE;
	}
	public void setEO_TYPE(int eO_TYPE) {
		EO_TYPE = eO_TYPE;
	}
	
	public Order() {
		
	}
	
	public Order(String eO_USER_ID, String eO_USER_NAME, String eO_USER_TEL, String eO_USER_ADDRESS, float eO_COST,
			String eO_CREATE_TIME, int eO_STATUS, int eO_TYPE) {
		super();
		EO_USER_ID = eO_USER_ID;
		EO_USER_NAME = eO_USER_NAME;
		EO_USER_TEL = eO_USER_TEL;
		EO_USER_ADDRESS = eO_USER_ADDRESS;
		EO_COST = eO_COST;
		EO_CREATE_TIME = eO_CREATE_TIME;
		EO_STATUS = eO_STATUS;
		EO_TYPE = eO_TYPE;
	}
	
	
	
	public Order(int eO_ID, String eO_USER_NAME, String eO_USER_ADDRESS, float eO_COST, String eO_CREATE_TIME,
			int eO_STATUS) {
		super();
		EO_ID = eO_ID;
		EO_USER_NAME = eO_USER_NAME;
		EO_USER_ADDRESS = eO_USER_ADDRESS;
		EO_COST = eO_COST;
		EO_CREATE_TIME = eO_CREATE_TIME;
		EO_STATUS = eO_STATUS;
	}
	
	
	public Order(int eO_ID, String eO_USER_ID, String eO_USER_NAME, String eO_USER_TEL, String eO_USER_ADDRESS,
			float eO_COST, String eO_CREATE_TIME, int eO_STATUS, int eO_TYPE) {
		super();
		EO_ID = eO_ID;
		EO_USER_ID = eO_USER_ID;
		EO_USER_NAME = eO_USER_NAME;
		EO_USER_TEL = eO_USER_TEL;
		EO_USER_ADDRESS = eO_USER_ADDRESS;
		EO_COST = eO_COST;
		EO_CREATE_TIME = eO_CREATE_TIME;
		EO_STATUS = eO_STATUS;
		EO_TYPE = eO_TYPE;
	}
	
	@Override
	public String toString() {
		return "Order [EO_ID=" + EO_ID + ", EO_USER_ID=" + EO_USER_ID + ", EO_USER_NAME=" + EO_USER_NAME
				+ ", EO_USER_TEL=" + EO_USER_TEL + ", EO_USER_ADDRESS=" + EO_USER_ADDRESS + ", EO_COST=" + EO_COST
				+ ", EO_CREATE_TIME=" + EO_CREATE_TIME + ", EO_STATUS=" + EO_STATUS + ", EO_TYPE=" + EO_TYPE + "]";
	}
	
	
}
