package com.dao;

import java.util.List;

import com.entity.Shopping;

//购物车
public interface ShoppingDao {
	
	//加入购物车
	public int join(Shopping sh);
	
	//修改购物车
	public int update(int sum,float total,int state,int gid);
	
	//删除购物车
	public int del(int gid);
	
	//查询购物车(用户id，1:未下单，2:已下单)
	public List gfind(int uid,int state);
	
	//查询重复加入购物车(用户id，1:未下单，2:已下单，商品id)
	public Shopping gfind(int uid,int state,int spid);
	
	//合计(用户id)
	public float getTotal(int uid);
	
	public int delsu(int uid);
}
