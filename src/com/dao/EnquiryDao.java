package com.dao;

import java.sql.SQLException;
import java.util.List;

import com.entity.Comment;

//留言
public interface EnquiryDao {
	//分页查询留言
	public List enfind(int page) throws SQLException;
	
	//查询总条数
	public int secon() throws SQLException;
	
	//添加留言
	public int addenq(Comment com) throws SQLException;
}
