package com.dao;

import java.sql.SQLException;
import java.util.List;

import com.entity.User;
//登入注册 用户进行登录注册
public interface UserDao {
	//登入
	public User login(String name,String pass) throws SQLException;
	
	//注册
	public int register(User user) throws SQLException;
	
	//id查询信息
	public User seid(int id) throws SQLException;
}
