package com.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.dao.ShoppingDao;
import com.dbutil.JDBCTool;
import com.entity.Shopping;

public class ShoppingDaoImpl implements ShoppingDao{

	QueryRunner runn = new QueryRunner();
	
	//加入购物车
	@Override
	public int join(Shopping sh) {
		Connection conn = JDBCTool.openconn();
		String sql = "insert into easybuy_shopping  " + 
				"(EO_USER_ID,EP_ID,EOD_QUANTITY,EOD_COST,EO_STATE)  " + 
				"value(?,?,?,?,?);";
		Object[] obj = new Object[5];
		obj[0]=sh.getEO_USER_ID();
		obj[1]=sh.getEP_ID();
		obj[2]=sh.getEOD_QUANTITY();
		obj[3]=sh.getEOD_COST();
		obj[4]=sh.getEO_STATE();
		long l= 0;
		try {
			l = runn.update(conn, sql, obj);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (int)l;
	}
	
	//修改购物车
	@Override
	public int update(int sum, float total, int state, int gid) {
		Connection conn = JDBCTool.openconn();
		String sql = "update easybuy_shopping set  " + 
				"EOD_QUANTITY=?,EOD_COST=?,EO_STATE=?  " + 
				"where EOD_ID=?;";
		long l= 0;
		try {
			l = runn.update(conn, sql, sum,total,state,gid);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (int)l;
	}
	
	//删除购物车
	@Override
	public int del(int gid) {
		Connection conn = JDBCTool.openconn();
		String sql = "delete from easybuy_shopping where EOD_ID=?;";
		long l= 0;
		try {
			l = runn.update(conn, sql,gid);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (int)l;
	}
	
	//查询购物车(用户id，1:未下单，2:已下单)
	@Override
	public List gfind(int uid,int state) {
		String snew="select * from easybuy_shopping where EO_USER_ID=? and EO_STATE=?;";
		Connection conn=JDBCTool.openconn();
		List list = null;
		try {
			list = runn.query(conn, snew, new BeanListHandler<Shopping>(Shopping.class),uid,state);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	//查询重复加入购物车(用户id，1:未下单，2:已下单，商品id)
	@Override
	public Shopping gfind(int uid,int state,int spid) {
		String snew="select * from easybuy_shopping where EO_USER_ID=? and EO_STATE=? and EP_ID=?;";
		Connection conn=JDBCTool.openconn();
		Shopping sh = null;
		try {
			sh = runn.query(conn, snew, new BeanHandler<Shopping>(Shopping.class),uid,state,spid);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sh;
	}

	//合计(用户id)
	@Override
	public float getTotal(int uid) {
		String snew="select sum(EOD_COST) from easybuy_shopping where EO_USER_ID=? and EO_STATE=1;";
		Connection conn=JDBCTool.openconn();
		double f = 0.0;
		try {
			f = runn.query(conn, snew, new ScalarHandler<Double>(),uid);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (float)f;
	}
	
	//支付成功后删除购物车
	@Override
	public int delsu(int uid) {
		// TODO Auto-generated method stub
		Connection conn = JDBCTool.openconn();
		String sql = "delete from easybuy_shopping where EO_USER_ID=?;";
		long l= 0;
		try {
			l = runn.update(conn, sql,uid);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (int)l;
	}

}
