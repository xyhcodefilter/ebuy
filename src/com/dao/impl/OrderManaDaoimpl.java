package com.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.dao.OrderManaDao;
import com.dbutil.JDBCTool;
import com.entity.Order;
import com.entity.User;

//订单
public class OrderManaDaoimpl implements OrderManaDao{
	QueryRunner runner = new QueryRunner();
	//分页查询所有
	@Override
	public List orall(int page) throws SQLException {
		// TODO Auto-generated method stub
		int n=(page-1)*5;
		String sef="select * from easybuy_order limit ?,5";
		Connection conn = JDBCTool.openconn();
		List list = runner.query(conn, sef, new BeanListHandler<Order>(Order.class), n);
		conn.close();
		return list;
	}
	
	//统计所有数量
	@Override
	public int couorall() throws SQLException {
		// TODO Auto-generated method stub
		String secoun="select count(*) from easybuy_order";
		Connection conn=JDBCTool.openconn();
		long olen=runner.query(conn, secoun, new ScalarHandler<Long>());
		conn.close();
		return (int)olen;
	}
	
	//删除指定的订单
	@Override
	public int deor(int id) throws SQLException {
		// TODO Auto-generated method stub
		String de="delete from easybuy_order where EO_ID=?";
		Connection conn=JDBCTool.openconn();
		int delor=runner.update(conn, de, id);
		conn.close();
		return delor;
	}
	
	//通过指定的ID得到订单信息
	@Override
	public List orconfig(int id) throws SQLException {
		// TODO Auto-generated method stub
		String orcif="select * from easybuy_order where EO_ID=?";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, orcif, new BeanListHandler<Order>(Order.class),id);
		conn.close();
		return list;
	}
	
	//修改订单
	@Override
	public int upor(Order or) throws SQLException {
		// TODO Auto-generated method stub
		String uor="update easybuy_order set EO_USER_NAME=?,EO_USER_ADDRESS=?,EO_COST=?,EO_CREATE_TIME=?,EO_STATUS=? where EO_ID=?";
		Connection conn=JDBCTool.openconn();
		Object obj[] =new Object[6];
		obj[0]=or.getEO_USER_NAME();
		obj[1]=or.getEO_USER_ADDRESS();
		obj[2]=or.getEO_COST();
		obj[3]=or.getEO_CREATE_TIME();
		obj[4]=or.getEO_STATUS();
		obj[5]=or.getEO_ID();
		int oraup=runner.update(conn, uor,obj);
		conn.close();
		return oraup;
	}
	
	//搜索id
	@Override
	public List sorid(int page,int id) throws SQLException {
		int n=(page-1)*5;
		// TODO Auto-generated method stub
		String sid="select * from easybuy_order where EO_ID like ? limit ?,5";
		String oid="%"+id+"%";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, sid, new BeanListHandler<Order>(Order.class),oid,n);
		conn.close();
		return list;
	}
	
	//搜索订单人
	@Override
	public List sornam(int page,String name) throws SQLException {
		// TODO Auto-generated method stub
		int n=(page-1)*5;
		String sna="select * from easybuy_order where EO_USER_NAME like ? limit ?,5";
		String ona="%"+name+"%";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, sna, new BeanListHandler<Order>(Order.class),ona,n);
		conn.close();
		return list;
	}
	
	//搜索ID、订单
	@Override
	public List soridna(int page,int id, String name) throws SQLException {
		// TODO Auto-generated method stub
		int n=(page-1)*5;
		String sidna="select * from easybuy_order where EO_ID like ? or EO_USER_NAME like ? limit ?,5";
		String oin="%"+id+"%";
		String ona="%"+name+"%";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, sidna, new BeanListHandler<Order>(Order.class),oin,ona,n);
		conn.close();
		return list;
	}
	
	//通过ID统计人数
	@Override
	public int secoid(int id) throws SQLException {
		// TODO Auto-generated method stub
		String idc="select count(*) from easybuy_order where EO_ID like ?";
		String sid="%"+id+"%";
		Connection conn=JDBCTool.openconn();
		long idle=runner.query(conn, idc, new ScalarHandler<Long>(),sid);
		conn.close();
		return (int)idle;
	}
	
	//通过na统计人数
	@Override
	public int secona(String na) throws SQLException {
		// TODO Auto-generated method stub
		String nac="select count(*) from easybuy_order where EO_USER_NAME like ?";
		String sna="%"+na+"%";
		Connection conn=JDBCTool.openconn();
		long nale=runner.query(conn, nac, new ScalarHandler<Long>(),sna);
		conn.close();
		return (int)nale;
	}
	
	//通过ID name统计
	@Override
	public int secoina(int id, String name) throws SQLException {
		// TODO Auto-generated method stub
		String sdn="select count(*) from easybuy_order where EO_ID like ? or EO_USER_NAME like ?";
		String on="%"+id+"%";
		String oa="%"+name+"%";
		Connection conn=JDBCTool.openconn();
		long naidl=runner.query(conn, sdn, new ScalarHandler<Long>(),on,oa);
		return (int)naidl;
	}
	
	//添加订单
	@Override
	public int addorder(Order or) throws SQLException {
		// TODO Auto-generated method stub
		String ador="insert into easybuy_order (EO_USER_ID,EO_USER_NAME,EO_USER_TEL,EO_USER_ADDRESS,EO_COST,EO_CREATE_TIME,EO_STATUS,EO_TYPE) value(?,?,?,?,?,?,?,?)";
		Object obj[]=new Object[8];
		obj[0]=or.getEO_USER_ID();
		obj[1]=or.getEO_USER_NAME();
		obj[2]=or.getEO_USER_TEL();
		obj[3]=or.getEO_USER_ADDRESS();
		obj[4]=or.getEO_COST();
		obj[5]=or.getEO_CREATE_TIME();
		obj[6]=or.getEO_STATUS();
		obj[7]=or.getEO_TYPE();
		Connection conn=JDBCTool.openconn();
		int ad=runner.update(conn, ador,obj);
		return ad;
	}
	
}
