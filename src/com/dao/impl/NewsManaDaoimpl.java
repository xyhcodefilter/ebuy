package com.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.dao.NewsManaDao;
import com.dbutil.JDBCTool;
import com.entity.News;
import com.entity.User;
//后台新闻管理
public class NewsManaDaoimpl implements NewsManaDao{
	QueryRunner runner=new QueryRunner();
	//分页查询所有新闻
	@Override
	public List newfind(int page) throws SQLException {
		// TODO Auto-generated method stub
		int n=(page-1)*6;
		String se="select * from easybuy_news limit ?,6";
		Connection conn=JDBCTool.openconn();
		List newsfind=runner.query(conn, se, new BeanListHandler<News>(News.class),n);
		conn.close();
		return newsfind;
	}
	
	//查询新闻的总数 
	@Override
	public int necoun() throws SQLException {
		// TODO Auto-generated method stub
		String ncont="select count(*) from easybuy_news";
		Connection conn=JDBCTool.openconn();
		long nelen=runner.query(conn,ncont,new ScalarHandler<Long>());
		conn.close();
		return (int)nelen;
	}
	
	//删除新闻
	@Override
	public int denew(int id) throws SQLException {
		// TODO Auto-generated method stub
		String dn="delete from easybuy_news where EN_ID=?";
		Connection conn=JDBCTool.openconn();
		int dsu=runner.update(conn, dn, id);
		conn.close();
		return dsu;
	}
	
	//通过ID查找新闻
	@Override
	public List neidse(int nid) throws SQLException {
		// TODO Auto-generated method stub
		String snew="select * from easybuy_news where EN_ID=?";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, snew, new BeanListHandler<News>(News.class),nid);
		conn.close();
		return list;
	}
	
	//修改新闻
	@Override
	public int upnew(News ne) throws SQLException {
		// TODO Auto-generated method stub
		String nup="update easybuy_news set EN_TITLE=?,EN_CONTENT=?,EN_CREATE_TIME=? where EN_ID=?";
		Connection conn=JDBCTool.openconn();
		Object obj[]=new Object[4];
		obj[0]=ne.getEN_TITLE();
		obj[1]=ne.getEN_CONTENT();
		obj[2]=ne.getEN_CREATE_TIME();
		obj[3]=ne.getEN_ID();
		int useu=runner.update(conn, nup, obj);
		conn.close();
		return useu;
	}
	
	//添加新闻
	@Override
	public int addnew(News ne) throws SQLException {
		// TODO Auto-generated method stub
		String nadd="insert into easybuy_news(EN_TITLE,EN_CONTENT,EN_CREATE_TIME) value(?,?,?)";
		Connection conn=JDBCTool.openconn();
		Object [] obje=new Object[3];
		obje[0]=ne.getEN_TITLE();
		obje[1]=ne.getEN_CONTENT();
		obje[2]=ne.getEN_CREATE_TIME();
		int adsu=runner.update(conn, nadd, obje);
		conn.close();
		return adsu;
	}
	
	

}
