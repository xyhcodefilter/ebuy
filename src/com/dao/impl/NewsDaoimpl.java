package com.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.dao.NewsDao;
import com.dbutil.JDBCTool;
import com.entity.News;
//新闻
public class NewsDaoimpl implements NewsDao{
	QueryRunner runner=new QueryRunner();
	//查最新新闻
	@Override
	public List news() throws SQLException {
		// TODO Auto-generated method stub
		String ne="select * from easybuy_news ORDER BY EN_CREATE_TIME desc limit 0,7;";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, ne, new BeanListHandler<News>(News.class));
		conn.close();
		return list;
	}
	
	//通过新闻id查
	@Override
	public List selid(int id) throws SQLException {
		// TODO Auto-generated method stub
		String seid="select * from easybuy_news where EN_ID=?";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, seid, new BeanListHandler<News>(News.class),id);
		conn.close();
		return list;
	}
	
	//查询老新闻
	@Override
	public List oldnew() throws SQLException {
		// TODO Auto-generated method stub
		String one="select * from easybuy_news ORDER BY EN_CREATE_TIME desc limit 7,7";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, one, new BeanListHandler<News>(News.class));
		conn.close();
		return list;
	}
	
	
}
