package com.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.dao.ProductCategoryDao;

import com.dbutil.JDBCTool;
import com.entity.Product;
import com.entity.ProductCategory;


public class ProductCategoryDaoimpl implements ProductCategoryDao{
	
	QueryRunner runn = new QueryRunner();
	
	//查询
	@Override
	public List query() {
		// TODO Auto-generated method stub
		Connection conn = JDBCTool.openconn();
		String sql = "select * from easybuy_product_category;";
		List list = null;
		try {
			list = runn.query(conn, sql, new BeanListHandler<ProductCategory>(ProductCategory.class));
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	//分页查询
	@Override
	public List pagequery(int p,int page) {
		Connection conn = JDBCTool.openconn();
		int n=(p-1)*page;
		String sql = "select * from easybuy_product_category limit ?,?;";
		List list = null;
		try {
			list = runn.query(conn, sql, new BeanListHandler<ProductCategory>(ProductCategory.class),n,page);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
	
	//查询单个商品类信息
	@Override
	public ProductCategory query(int id) {
		Connection conn = JDBCTool.openconn();
		String sql = "select * from easybuy_product_category where EPC_ID=?;";
		ProductCategory p = null;
		try {
			p = runn.query(conn, sql, new BeanHandler<ProductCategory>(ProductCategory.class),id);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return p;
	}
	
	//查询多个商品类信息（父类ID）
	@Override
	public List querylist(int id) {
		Connection conn = JDBCTool.openconn();
		String sql = "select * from easybuy_product_category where EPC_PARENT_ID=?;";
		List list = null;
		try {
			list = runn.query(conn, sql, new BeanListHandler<ProductCategory>(ProductCategory.class),id);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}

	//添加
	@Override
	public int add(String name,int eid) {
		// TODO Auto-generated method stub
		Connection conn = JDBCTool.openconn();
		String sql = "insert into easybuy_product_category(EPC_NAME,EPC_PARENT_ID) value(?,?);";
		long l = 0;
		try {
			l = runn.update(conn, sql, name,eid);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (int)l;
	}
	
	//修改
	@Override
	public int update(int id,String name,int eid) {
		Connection conn = JDBCTool.openconn();
		String sql = "update easybuy_product_category set EPC_NAME=?,EPC_PARENT_ID=? where EPC_ID=?;";
		long l = 0;
		try {
			l = runn.update(conn, sql,name,eid,id);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (int)l;
	}
	
	//删除
	@Override
	public int del(int id) {
		Connection conn = JDBCTool.openconn();
		String sql = "delete from easybuy_product_category where EPC_ID=?;";
		long l = 0;
		try {
			l = runn.update(conn, sql, id);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (int)l;
	}
	
	//统计
	@Override
	public int count() {
		String seco="select count(*) from easybuy_product_category";
		Connection conn=JDBCTool.openconn();
		long lo=0;
		try {
			lo = runn.query(conn,seco,new ScalarHandler<Long>());
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return (int)lo;
	}
	
	//通过id查询有没有这个类
	@Override
	public List secate(int id) {
		// TODO Auto-generated method stub
		String se="select * from easybuy_product where EPC_CHILD_ID=?";
		Connection conn=JDBCTool.openconn();
		List list=null;
		try {
			list=runn.query(conn, se,new BeanListHandler<Product>(Product.class),id);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
}
