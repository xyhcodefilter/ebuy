package com.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.dao.EnquiryManaDao;
import com.dbutil.JDBCTool;
import com.entity.Comment;
//后台留言管理
public class EnquiryManaDaoimpl implements EnquiryManaDao{
	QueryRunner runner=new QueryRunner();
	//分页查询留言
	@Override
	public List enqfin(int page) throws SQLException {
		// TODO Auto-generated method stub
		int pa = (page - 1) * 6;
		String enf = "select * from easybuy_comment limit ?,6";
		Connection conn = JDBCTool.openconn();
		List list = runner.query(conn, enf, new BeanListHandler<Comment>(Comment.class), pa);
		conn.close();
		return list;
	}
	
	//回复留言
	@Override
	public int upenq(Comment con) throws SQLException {
		// TODO Auto-generated method stub
		String up="update easybuy_comment set EC_CONTENT=?,EC_CREATE_TIME=?,EC_REPLY=?,EC_REPLY_TIME=?,EC_NICK_NAME=? where EC_ID=?";
		Connection conn=JDBCTool.openconn();
		Object obj[]=new Object[6];
		obj[0]=con.getEC_CONTENT();
		obj[1]=con.getEC_CREATE_TIME();
		obj[2]=con.getEC_REPLY();
		obj[3]=con.getEC_REPLY_TIME();
		obj[4]=con.getEC_NICK_NAME();
		obj[5]=con.getEC_ID();
		int seu= runner.update(conn, up, obj);
		conn.close();
		return seu;
	}
	
	//删除留言
	@Override
	public int deenq(int id) throws SQLException {
		// TODO Auto-generated method stub
		String de="delete from easybuy_comment where EC_ID=?";
		Connection conn=JDBCTool.openconn();
		int deenq=runner.update(conn, de, id);
		conn.close();
		return deenq;
	}
	
	//统计所有的留言数量
	@Override
	public int enqcou() throws SQLException {
		// TODO Auto-generated method stub
		String so="select count(*) from easybuy_comment";
		Connection conn=JDBCTool.openconn();
		long enqc=runner.query(conn, so, new ScalarHandler<Long>());
		conn.close();
		return (int)enqc;
	}
	
	//通过ID查找留言
	@Override
	public List fenq(int id) throws SQLException {
		// TODO Auto-generated method stub
		String seus="select * from easybuy_comment where EC_ID=?";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, seus, new BeanListHandler<Comment>(Comment.class),id);
		conn.close();
		return list;
	}
	
	

}
