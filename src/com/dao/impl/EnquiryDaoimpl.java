package com.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.dao.EnquiryDao;
import com.dbutil.JDBCTool;
import com.entity.Comment;
//留言
public class EnquiryDaoimpl implements EnquiryDao{
	QueryRunner runner=new QueryRunner();
	
	//留言分页
	@Override
	public List enfind(int page) throws SQLException {
		// TODO Auto-generated method stub
		int n=(page-1)*3;
		String se="select * from easybuy_comment limit ?,3";
		Connection conn=JDBCTool.openconn();
		List finli=runner.query(conn, se, new BeanListHandler<Comment>(Comment.class),n);
		conn.close();
		return finli;
	}
	
	//得到总条数
	@Override
	public int secon() throws SQLException {
		// TODO Auto-generated method stub
		String seco="select count(*) from easybuy_comment";
		Connection conn=JDBCTool.openconn();
		long lo=runner.query(conn,seco,new ScalarHandler<Long>());
		conn.close();
		return (int)lo;
	}
	//添加留言
	@Override
	public int addenq(Comment com) throws SQLException {
		// TODO Auto-generated method stub
		String add="insert into easybuy_comment(EC_CONTENT,EC_CREATE_TIME,EC_REPLY,EC_REPLY_TIME,EC_NICK_NAME) value(?,?,?,?,?)";
		Connection conn=JDBCTool.openconn();
		Object [] obje=new Object[5];
		obje[0]=com.getEC_CONTENT();
		obje[1]=com.getEC_CREATE_TIME();
		obje[2]=com.getEC_REPLY();
		obje[3]=com.getEC_REPLY_TIME();
		obje[4]=com.getEC_NICK_NAME();
		int a=runner.update(conn, add, obje);
		conn.close();
		return a;
	}

}
