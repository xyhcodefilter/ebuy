package com.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.dao.UserManaDao;
import com.dbutil.JDBCTool;
import com.entity.News;
import com.entity.User;

//后台用户管理 
public class UserManaDaoimpl implements UserManaDao {
	QueryRunner runner = new QueryRunner();
	
	//分页查询用户
	@Override
	public List muser(int page) throws SQLException {
		// TODO Auto-generated method stub
		int pa = (page - 1) * 6;
		String mus = "select * from easybuy_user where EU_STATUS=1 limit ?,6";
		Connection conn = JDBCTool.openconn();
		List list = runner.query(conn, mus, new BeanListHandler<User>(User.class), pa);
		conn.close();
		return list;
	}
	
	//统计所有用户
	@Override
	public int cous() throws SQLException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String cose = "select count(*) from easybuy_user where EU_STATUS=1";
		Connection conn = JDBCTool.openconn();
		long scoup = runner.query(conn, cose, new ScalarHandler<Long>());
		conn.close();
		return (int) scoup;
	}
	
	//删除用户
	@Override
	public int usde(int id) throws SQLException {
		// TODO Auto-generated method stub
		String de="delete from easybuy_user where EU_USER_ID=?";
		Connection conn=JDBCTool.openconn();
		int deus=runner.update(conn, de, id);
		conn.close();
		return deus;
	}
	
	//添加用户
	@Override
	public int aduser(User user) throws SQLException {
		// TODO Auto-generated method stub
		String sad="insert into easybuy_user (EU_USER_Account,EU_USER_NAME,EU_PASSWORD,EU_SEX,EU_BIRTHDAY,EU_IDENTITY_CODE,EU_EMAIL,EU_MOBILE,EU_ADDRESS,EU_STATUS) value(?,?,?,?,?,?,?,?,?,?)";
		Connection conn=JDBCTool.openconn();
		Object [] obje=new Object[10];
		obje[0]=user.getEU_USER_Account();
		obje[1]=user.getEU_USER_NAME();
		obje[2]=user.getEU_PASSWORD();
		obje[3]=user.getEU_SEX();
		obje[4]=user.getEU_BIRTHDAY();
		obje[5]=user.getEU_IDENTITY_CODE();
		obje[6]=user.getEU_EMAIL();
		obje[7]=user.getEU_MOBILE();
		obje[8]=user.getEU_ADDRESS();
		obje[9]=user.getEU_STATUS();
		int adsu=runner.update(conn, sad,obje);
		conn.close();
		return adsu;
	}
	
	//通过ID查找用户
	@Override
	public List seruser(int uid) throws SQLException {
		// TODO Auto-generated method stub
		String seus="select * from easybuy_user where EU_USER_ID=?";
		Connection conn=JDBCTool.openconn();
		List list=runner.query(conn, seus, new BeanListHandler<User>(User.class),uid);
		conn.close();
		return list;
	}
	
	//修改
	@Override
	public int upuser(User user) throws SQLException {
		// TODO Auto-generated method stub
		String up="update easybuy_user set EU_USER_Account=?,EU_USER_NAME=?,EU_PASSWORD=?,EU_SEX=?,EU_BIRTHDAY=?,EU_MOBILE=?,EU_ADDRESS=? where EU_USER_ID=?";
		Connection conn=JDBCTool.openconn();
		Object obj[]=new Object[8];
		obj[0]=user.getEU_USER_Account();
		obj[1]=user.getEU_USER_NAME();
		obj[2]=user.getEU_PASSWORD();
		obj[3]=user.getEU_SEX();
		obj[4]=user.getEU_BIRTHDAY();
		obj[5]=user.getEU_MOBILE();
		obj[6]=user.getEU_ADDRESS();
		obj[7]=user.getEU_USER_ID();
		int useu=runner.update(conn, up, obj);
		conn.close();
		return useu;
	}
	
}
