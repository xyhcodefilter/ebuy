package com.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.dao.UserDao;
import com.dbutil.JDBCTool;
import com.entity.User;

//登入注册
public class UserDaoimpl implements UserDao{
	QueryRunner runner=new QueryRunner();
	
	//登入
	@Override
	public User login(String name, String pass) throws SQLException {
		// TODO Auto-generated method stub
		String selo="select * from easybuy_user where EU_USER_Account=? and EU_PASSWORD=?";
		Connection conn=JDBCTool.openconn();
		User us=runner.query(conn, selo, new BeanHandler<User>(User.class),name,pass);
		conn.close();
		return us;
	}
	//注册
	@Override
	public int register(User user) throws SQLException {
		// TODO Auto-generated method stub
		String re="insert into easybuy_user(EU_USER_Account,EU_USER_NAME,EU_PASSWORD,EU_SEX,EU_BIRTHDAY,EU_IDENTITY_CODE,EU_EMAIL,EU_MOBILE,EU_ADDRESS,EU_STATUS) value(?,?,?,?,?,?,?,?,?,?)";
		Connection conn=JDBCTool.openconn();
		Object obj[]=new Object[10];
		obj[0]=user.getEU_USER_Account();
		obj[1]=user.getEU_USER_NAME();
		obj[2]=user.getEU_PASSWORD();
		obj[3]=user.getEU_SEX();
		obj[4]=user.getEU_BIRTHDAY();
		obj[5]=user.getEU_IDENTITY_CODE();
		obj[6]=user.getEU_EMAIL();
		obj[7]=user.getEU_MOBILE();
		obj[8]=user.getEU_ADDRESS();
		obj[9]=user.getEU_STATUS();
		int resu=runner.update(conn, re, obj);
		conn.close();
		return resu;
	}
	
	//id查询
	@Override
	public User seid(int id) throws SQLException {
		// TODO Auto-generated method stub
		String se="select * from easybuy_user where EU_USER_ID=?";
		Connection conn=JDBCTool.openconn();
		User us=runner.query(conn, se,new BeanHandler<User>(User.class),id);
		conn.close();
		return us;
	}
}
