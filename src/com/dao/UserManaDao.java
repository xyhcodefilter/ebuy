package com.dao;

import java.sql.SQLException;
import java.util.List;

import com.entity.User;
//后台用户管理
public interface UserManaDao {
	// 查询用户分页
	public List muser(int page) throws SQLException;

	// 统计所有的用户
	public int cous() throws SQLException;
	
	//删除用户
	public int usde(int id) throws SQLException;
	
	//添加用户 
	public int aduser(User user) throws SQLException;
	
	//通过ID查找用户
	public List seruser(int uid) throws SQLException;
	
	//修改用户
	public int upuser(User user) throws SQLException;
}
