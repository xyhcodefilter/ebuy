package com.dao;

import java.util.List;

import com.entity.Product;
//商品
public interface ProductDao {
	//添加
		public int add(Object... param);
		
		//修改
		public int update(Object... param);
		
		//删除
		public int del(int id);
		
		//查询
		public List query();
		public List pagequery(int p,int page);   //分页查询
		public List querystoer(int p,int page);   //查询多个指定范围商品信息
		public List querylist(int id);   //查询多个个商品信息
		public Product query(int id);   //查询商品类多个商品信息
		public List fquery(int p,int id,int page);  //商品类父id分页查询
		
		public List Sale();
		
		//统计
		public int count();
		public int count(int id);  //id统计
		
}
