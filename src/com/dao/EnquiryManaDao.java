package com.dao;

import java.sql.SQLException;
import java.util.List;

import com.entity.Comment;

//留言管理
public interface EnquiryManaDao {
	//分页查询留言
	public List enqfin(int page) throws SQLException;
	
	//回复留言
	public int upenq(Comment con) throws SQLException;
	
	//删除ID留言
	public int deenq(int id) throws SQLException;
	
	//统计所有的留言 
	public int enqcou() throws SQLException;
	
	//通过ID查找留言
	public List fenq(int id) throws SQLException;
}
