package com.dao;

import java.sql.SQLException;
import java.util.List;

import com.entity.Order;

//订单
public interface OrderManaDao {
	//分页查询订单
	public List orall(int page) throws SQLException; 
	
	//统计所有的得到数量
	public int couorall() throws SQLException;
	
	//删除指定的订单
	public int deor(int id) throws SQLException;
	
	//通过指定ID得到订单信息
	public List orconfig(int id) throws SQLException;
	
	//修改订单
	public int upor(Order or) throws SQLException;
	
	//搜索订单id
	public List sorid(int page,int id) throws SQLException;
	
	//统计搜索ID的人数
	public int secoid(int id) throws SQLException;
	
	//搜索订货人
	public List sornam(int page,String name) throws SQLException;
	
	//通过订货人统计人数
	public int secona(String na) throws SQLException;
	
	//搜索订单ID 订货人
	public List soridna(int page,int id,String name) throws SQLException;
	
	//通过ID 订单人统计
	public int secoina(int id,String name) throws SQLException;
	
	//添加订单
	public int addorder(Order or) throws SQLException;
}
