package com.dao;

import java.sql.SQLException;
import java.util.List;

import com.entity.News;
//新闻管理
public interface NewsManaDao {
	//新闻分页查询
	public List newfind(int page)  throws SQLException;
	
	//新闻统计总数
	public int necoun() throws SQLException;
	
	//删除新闻
	public int denew(int id) throws SQLException;
	
	//通过ID查找新闻
	public List neidse(int nid) throws SQLException;
	
	//修改新闻
	public int upnew(News ne) throws SQLException;
	
	//添加新闻
	public int addnew(News ne) throws SQLException;
}
