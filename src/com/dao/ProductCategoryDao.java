package com.dao;

import java.util.List;

import com.entity.ProductCategory;

public interface ProductCategoryDao {
	//添加
		public int add(String name,int eid);
		
		//修改
		public int update(int id,String name,int eid);
		
		//删除
		public int del(int id);
		
		//统计
		public int count();
		
		//查询
		public List query();
		public List pagequery(int p,int page);   //分页查询
		public ProductCategory query(int id);   //查询单个商品类信息
		public List querylist(int id);   //查询多个商品类信息（父类ID）
		
		public List secate(int id);//查询当前类下有没有数据
}
