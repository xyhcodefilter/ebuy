package com.dao;

import java.sql.SQLException;
import java.util.List;
//新闻
public interface NewsDao {
	//最新新闻
	public List news() throws SQLException;
	
	//通过id查找新闻
	public List selid(int id) throws SQLException;
	
	//得到老新闻
	public List oldnew() throws SQLException;
}
